#!/usr/bin/env python3
import argparse

parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('--random_seed', type=int, default=1, help='set dynet and python random seeds for replicability; 0 implies truly random seed')
parser.add_argument('--no_sleep', action='store_true', help='switch off random startup sleep (useful on NFS-based clusters for multi-processing)')
parser.add_argument('--char_embed_size', type=int, default=20)
parser.add_argument('--line_seq_layers', type=int, default=2)
parser.add_argument('--line_seq_hidden_size', type=int, default=20)
# TODO: Option um attention vs. final output (potentially more possibilities (mean, max, ...))
parser.add_argument('--line_attention_size', type=int, default=20)
parser.add_argument('--use_audio', action='store_true')
parser.add_argument('--ignore_text', action='store_true')
parser.add_argument('--no_pretraining', action='store_true')
parser.add_argument('--normalize_acoustics', type=bool, default=True)
parser.add_argument('--audio_seq_step', type=int, default=10, help='how many feature frames to combine; will be overridden by pickle\'d value') # 100ms
parser.add_argument('--audio_seq_layers', type=int, default=2)
parser.add_argument('--audio_seq_hidden_size', type=int, default=30)
parser.add_argument('--audio_attention_size', type=int, default=25)
parser.add_argument('--line_annotation_suffix', type=str, default='/txt.lab.anno', help='file suffix for line-by-line annotations')
parser.add_argument('--dropout', type=float, default=0.2, help='dropout probability (applied on all layers)')
parser.add_argument('--iterations', type=int, default=30, help='training iterations for the network; use fewer iterations without dropout')
#parser.add_argument('--test_size', type=int, default=1, help='size of each test set during cross-fold validation')
parser.add_argument('--test_folds', type=int, default=10, help='number of folds during cross-fold validation')
parser.add_argument('--data_path', type=str, default='', help='path to data files; UNUSED if using pickled data') # /home/timo/Dropbox/Prosodische_Muster_Gedicht/
parser.add_argument('--trials', type=int, default=1, help='the number of times to run classification for each file to balance out effects from random initialization')
parser.add_argument('--store_pickle', default=True, action='store_true', help='store a pickle of the loaded plain text input poem file')
parser.add_argument('--dynet_autobatch', type=int, default=1)
parser.add_argument('input_poems_file', nargs='+', help='specification of poems and assigned classes; either plain text (see README) or pickle (if suffix is .pickle)')
args = parser.parse_args()
args.poem_lab_suffix = '/txt.lab'
args.ignore_anno = False
args.ignore_pause = True
#args.ignore_audio = True
print(args)

import dynet_config
dynet_config.set(random_seed=args.random_seed, autobatch=args.dynet_autobatch, mem=1536)
import dynet as dy
import datetime
from random import shuffle, random, seed
if args.random_seed != 0:
    seed(args.random_seed)
from scipy.stats import binom_test
from time import sleep
from colors import color # color on the commandline!, corresponding module is called "ansicolors" in pip
import deeplyriklib as dll
import numpy as np

AUDIO_DIM = 2 * (13 + 7) # 13 mfcc, 7 ffv, means and stddev of each

args.AUDIO_DIM = AUDIO_DIM
dll.args = args

if not args.no_sleep:
    sleeptime = random() * 8
    print("sleeping for {:.3} s".format(sleeptime))
    sleep(sleeptime)


def attend(output_vectors, params, attentionName, attentionStore=None):
    MLP_W = params[attentionName + "_MLP_W"]
    MLP_bias = params[attentionName + "_MLP_bias"]
    U = params[attentionName + "_U"]
    output_matrix = dy.concatenate_cols(output_vectors)
    dy.dropout(output_matrix, args.dropout)
    unnormalized = dy.transpose(U * dy.tanh(dy.colwise_add(MLP_W * output_matrix, MLP_bias)))
    return unnormalized


def get_line_loss(className, inputText, inputAudio, pauseAudio, lineAnnotation, params):
    output = compute_line_network_output(inputText, inputAudio, pauseAudio, lineAnnotation, params, isTrain=True)
    loss = dy.pickneglogsoftmax(output, lineAnnotation["punctuationafter"])
    return loss


def compute_line_network_decision(inputText, inputAudio, pauseAudio, lineAnnotation, params, isTrain=False, attentionStore=None):
    return dy.softmax(compute_line_network_output(inputText, inputAudio, pauseAudio, lineAnnotation, params, isTrain, attentionStore))


def compute_audio_representation(inputAudio, lstmTypeName, params, isTrain):
    inputAudio = [[0 for x in range(args.AUDIO_DIM)]] + inputAudio + [[0 for x in range(args.AUDIO_DIM)]]
    audioTensor = dy.inputTensor(inputAudio)
    fwd_vectors = dll.run_rnn(params, "fwd" + lstmTypeName + "LSTM", audioTensor, isTrain)
    bwdAudioTensor = dy.inputTensor(list(reversed(inputAudio)))
    bwd_vectors = dll.run_rnn(params, "bwd" + lstmTypeName + "LSTM", bwdAudioTensor, isTrain)
    return [dy.concatenate(list(p)) for p in zip(fwd_vectors, bwd_vectors)]


def encenc_attend(input_mat, state, w1dt, params):
    w2dt = params["audioAtt_MLP_W2"] * dy.concatenate(list(state.s()))
    unnormalized = dy.transpose(params["audioAtt_U"] * dy.tanh(dy.colwise_add(w1dt, w2dt)))
    att_weights = dy.softmax(unnormalized)
    context = input_mat * att_weights
    return context


def encenc_line_network_output(inputText, inputAudio, pauseAudio, lineAnnotation, params, isTrain=False, attentionStore=None):
    audio_state_vectors = compute_audio_representation(inputAudio, "Audio", params, isTrain)
    input_mat = dy.concatenate_cols(audio_state_vectors)
    w1dt = None
    inputEmbds = dll.lookup_text_for_rnn(inputText, params)
    last_output_embeddings = inputEmbds[0]
    all_fwd_states = []
    rnn = params["fwdCharLSTM"]
    if isTrain:
        rnn.set_dropout(args.dropout)
    s = rnn.initial_state().add_input(dy.concatenate([dy.vecInput(2*args.audio_seq_hidden_size), last_output_embeddings]))
    for char in inputEmbds[1:]:
        w1dt = w1dt or params["audioAtt_MLP_W1"] * input_mat
        vector = dy.concatenate([encenc_attend(input_mat, s, w1dt, params), last_output_embeddings])
        s = s.add_input(vector)
        all_fwd_states.append(dy.concatenate(list(s.s())))
        last_output_embeddings = char
    output = attend(all_fwd_states, params, "charAtt", attentionStore)
    return output


def standard_line_network_output(inputText, inputAudio, pauseAudio, lineAnnotation, params, isTrain=False, attentionStore=None):
    # process character text:
    fwd_char_output = dll.run_char_rnn(params, "fwdCharLSTM", inputText, isTrain)
    bwd_char_output = dll.run_char_rnn(params, "bwdCharLSTM", list(reversed(inputText)), isTrain)
    full_output = [dy.concatenate(list(p)) for p in zip(fwd_char_output, list(reversed(bwd_char_output)))]
    output = attend(full_output, params, "charAtt", attentionStore)
    # TODO: implement audio
    return output


def compute_line_network_output(inputText, inputAudio, pauseAudio, lineAnnotation, params, isTrain=False, attentionStore=None):
    dy.renew_cg()
    if args.use_audio:
        return encenc_line_network_output(inputText, inputAudio, pauseAudio, lineAnnotation, params, isTrain, attentionStore)
    else:
        return standard_line_network_output(inputText, inputAudio, pauseAudio, lineAnnotation, params, isTrain, attentionStore)


def trainOnLines(trainLines, trainer, params, iterations):
    for iteration in range(iterations):
        shuffle(trainLines)
        i = 0
        aggrloss = 0
        for (_, line) in trainLines:
            if line[3]["middlepunctuation"] == 'j':
                # !!! need to pass empty annotation to compute_line_network_decision (potentially in the future: remove our class and pass everything else)
                loss = get_line_loss(None, line[0], line[1], None, line[3], params)
                loss.backward()
                loss_value = loss.value()
                aggrloss += loss_value
                trainer.update()
                i += 1
        if i > 0:
            print("per-line iteration:\t{:02},\tavg. loss:\t{:.3}\r".format(iteration, aggrloss/i), end='')
    print() # add newline (above print does not print newlines)


def testOnLines(testLines, params, testResults):
    testCorrect = 0
    for (_, line) in testLines:
        if line[3]["middlepunctuation"] == 'j':
            # a "line" contains both text and audio, but not the class label
            #!!! need to pass empty annotation to compute_line_network_decision (potentially in the future: remove our class and pass everything else)
            attention_activities = compute_line_network_decision(line[0], line[1], None, None, params)
            best = max([(v,i) for (i,v) in enumerate(attention_activities.value())])
            estimatedClass = best[1]
#            estimatedName = params["int2class"][estimatedClass]
            realClass = line[3]["punctuationafter"]
            correct = estimatedClass == realClass
            testResults["total"] += 1
            testResults["correct"] += 1 if correct else 0
            testCorrect += 1 if correct else 0
            loss = dy.pickneglogsoftmax(attention_activities, best[1])
            print("correct: {},\tclassified as:\t{},\tloss:\t{:5.3f}\t".format(realClass, estimatedClass, loss.value()), end='')
            colors = dll.colorsForWeights(attention_activities)
            for (char, charcolor) in zip(' ' + line[0] + ' ', colors):
                print(color(char, fg='white', bg=charcolor), end='')
            print()
    return testCorrect


def trainAndTest(trainPoems, pc, params, iterations=50, testPoems=[], testResults=[]):
    #print(color("trainAndTest >>>>>>> Start ...", fg='green'))
    trainer = dy.AdamTrainer(pc)
    
    poemsCorrect = 0
    ### unfold training poems into individual lines
    trainLines = [x for x in unfoldToLineData(trainPoems) if x[1][3]["middlepunctuation"] == 'j' and len(x[1][0]) < 50]
    testLines = [x for x in unfoldToLineData(testPoems) if x[1][3]["middlepunctuation"] == 'j' and len(x[1][0]) < 50]

    trainOnLines(trainLines, trainer, params, iterations)
    # perform per-line evaluation and print (brief) stats
    linesCorrect = testOnLines(testLines, params, testResults=testResults["linesIndiv"])
    print("lines: {}/{} ({:5.1f}%)".format(linesCorrect, len(testLines), 100.0*linesCorrect/len(testLines)))

def unfoldToLineData(poems):
    # ignore lines without text ("")
    # also ignore lines that have "multiple" punctuation in the middle
    return [(p.getClass(), l) for p in poems for l in p.getLinesAndAudio() if l[0] != "" and l[3]["middlepunctuation"] != "multiple"]


def main():
    """start with one tab-separated-values file as parameter. 
       Each line should contain info for one poem: 
       the poem's class, path to file containing the poem's text"""
    print(color("started: {}".format(datetime.datetime.now()), fg='green'))
    print("loading input from [{}]".format(', '.join(args.input_poems_file)))

    args.ignore_anno = True
    (classData, observed_char_types) = dll.loadFromFiles(args.input_poems_file, poemFormat='lab')
    linesWithoutPunctuation = linesWithMultiplePunctuation = linesTotal = 0
    for poems in classData.values():
        for poem in poems:
            poem.discardAndAnnotatePunctuation()
            linesTotal += len(poem.textLines)
            linesWithoutPunctuation += len([0 for x in poem.annotations if x["middlepunctuation"] == "n"])
            linesWithMultiplePunctuation += len([0 for x in poem.annotations if x["middlepunctuation"] == "multiple"])
    print("{} out of {} without punctuation".format(linesWithoutPunctuation, linesTotal))
    print("{} out of {} with multiple punctuation".format(linesWithMultiplePunctuation, linesTotal))

    # classData contains a dict; every class is a key with lists where entries represent poems
    #print(len(classData), NUM_CLASSES, CHAR_TYPES)
    CHAR_TYPES = ['\t', ' ', '!', '"', '#', '$', '%', '&', "'", '(', ')', '*', '+', ',', '-', '.', '/', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ':', ';', '<', '=', '>', '?', '@', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '[', ']', '^', '_', '`', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '|', '~', '©', '«', '¬', '\xad', '¯', '°', '´', '·', 'º', '»', 'Ä', 'Ç', 'É', 'Ó', 'Ö', '×', 'Ø', 'Ü', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'ö', 'ø', 'ú', 'û', 'ü', 'ā', 'ą', 'ē', 'ı', 'ł', 'ŉ', 'ŋ', 'š', 'ţ', 'ū', 'ż', 'ſ', 'ɘ', '̈', 'Α', 'Γ', 'Θ', 'Ι', 'Κ', 'Ν', 'Ο', 'Σ', 'Τ', 'Υ', 'Ω', 'α', 'β', 'γ', 'δ', 'ε', 'η', 'ι', 'λ', 'μ', 'ν', 'ο', 'π', 'σ', 'τ', 'ω', 'ϑ', 'ϱ', 'Б', 'В', 'Н', 'С', 'Ы', 'Э', 'а', 'в', 'г', 'д', 'е', 'ж', 'и', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'х', 'ц', 'ш', 'щ', 'ы', 'ь', 'э', 'أ', 'ا', 'ة', 'ر', 'ش', 'ض', 'ف', 'ἐ', 'Ἔ', 'ἣ', 'ὅ', 'Ὦ', 'ὴ', 'ώ', 'ῆ', 'ῖ', '‒', '–', '—', '‘', '’', '‚', '“', '”', '„', '†', '•', '…', '‹', '›', '№', '→', '−', '∙', '√', '│', '◊', '・', 'ﬂ', '<EOL>']
    for char in observed_char_types:
        assert char in CHAR_TYPES, 'character "{}" in input data cannot be mapped to known char types'.format(char)
    INPUT_DIM = len(CHAR_TYPES)

    params = {}
    params["int2char"] = list(CHAR_TYPES)
    params["char2int"] = {c:i for i,c in enumerate(CHAR_TYPES)}
    allPoems = [x[1] for x in dll.unfoldClassData(classData)]
    numPoems = len(allPoems)


    allRuns = args.test_folds * args.trials
    runIndex = 0
    testResults = { "linesIndiv" : { "total" : 0, "correct" : 0 },
                    "linesGlobal" : { "total" : 0, "correct" : 0 },
                    "poems" : { "total" : 0, "correct" : 0 }
                  }
    assert args.test_folds <= numPoems, "test set size is too large for the given number of poems: {} vs. {}".format(args.test_folds, numPoems)

    for i in range(args.trials):
        # shuffle poems in each class
        folds = [[] for x in range(args.test_folds)]
        # for every class, shuffle items in class, add items to buckets, shuffle buckets, add buckets to folds
        for (poemClass, poems) in classData.items():
            shuffle(poems)
            classFolds = [[] for x in range(args.test_folds)]
            for i in range(len(poems)):
                classFolds[i % args.test_folds].append(poems[i])
            shuffle(classFolds)
            for i in range(len(folds)):
                folds[i].extend(classFolds[i])
#        print(folds)
        for testPoems in folds:
            runIndex += 1
            print('training run {} of {} ({:5.1f}% done)'.format(runIndex, allRuns, 100*(runIndex-1) / allRuns))

            ### set up dynet anew
            pc = dy.Model()
            # per-line text representation
            params["fwdCharLSTM"] = dy.GRUBuilder(args.line_seq_layers, args.char_embed_size + (2 * args.audio_seq_hidden_size if args.use_audio else 0), args.line_seq_hidden_size, pc)
            params["bwdCharLSTM"] = dy.GRUBuilder(args.line_seq_layers, args.char_embed_size + (2 * args.audio_seq_hidden_size if args.use_audio else 0), args.line_seq_hidden_size, pc)
            # per-line audio representation
            params["fwdAudioLSTM"] = dy.GRUBuilder(args.audio_seq_layers, AUDIO_DIM, args.audio_seq_hidden_size, pc)
            params["bwdAudioLSTM"] = dy.GRUBuilder(args.audio_seq_layers, AUDIO_DIM, args.audio_seq_hidden_size, pc)
            if (args.no_pretraining):
                params["char_lookup"] = pc.add_lookup_parameters((INPUT_DIM, args.char_embed_size))
                params["charAtt_MLP_W"] = pc.add_parameters((args.line_attention_size, args.line_seq_hidden_size * 2))
                params["charAtt_MLP_bias"] = pc.add_parameters((args.line_attention_size))
                params["charAtt_U"] = pc.add_parameters((1, args.line_attention_size))
                params["audioAtt_MLP_W1"] = pc.add_parameters((args.audio_attention_size, args.audio_seq_hidden_size * 2))
                params["audioAtt_MLP_W2"] = pc.add_parameters((args.audio_attention_size, args.line_seq_hidden_size * args.line_seq_layers))
                params["audioAtt_MLP_bias"] = pc.add_parameters((args.audio_attention_size))
                params["audioAtt_U"] = pc.add_parameters((1, args.audio_attention_size))
            else:
                params["char_lookup"] = pc.load_lookup_param('pretraining/pretraining.dypar', '/charLookup')
                params["fwdCharLSTM"].param_collection().populate('pretraining/pretraining.dypar', '/gru-builder/')
                params["bwdCharLSTM"].param_collection().populate('pretraining/pretraining.dypar', '/gru-builder_1/')
                params["charAtt_MLP_W"] = pc.load_param('pretraining/pretraining.dypar', '/charAttMLPW')
                params["charAtt_MLP_bias"] = pc.load_param('pretraining/pretraining.dypar', '/charAttMLPbias')
                params["charAtt_U"] = pc.load_param('pretraining/pretraining.dypar', '/charAttU')
                params["fwdAudioLSTM"].param_collection().populate('pretraining/audioPretraining.dypar', '/gru-builder/')
                params["bwdAudioLSTM"].param_collection().populate('pretraining/audioPretraining.dypar', '/gru-builder_1/')
                params["audioAtt_MLP_W"] = pc.load_param('pretraining/audioPretraining.dypar', '/audioAttMLPW')
                params["audioAtt_MLP_bias"] = pc.load_param('pretraining/audioPretraining.dypar', '/audioAttMLPbias')
                params["audioAtt_U"] = pc.load_param('pretraining/audioPretraining.dypar', '/audioAttU')

            trainPoems = []
            for fold in folds:
                if fold != testPoems:
                    trainPoems.extend(fold)
            trainAndTest(trainPoems, pc, params, iterations=args.iterations, testPoems=testPoems, testResults=testResults)
#            print(testResults)
        majorityProportion = max([len(x) for x in classData.values()]) / numPoems
        print(testResults)
        print("{} out of {} correct ({:5.1f}%), p<{:.3}".format(
          testResults["linesIndiv"]["correct"], 
          testResults["linesIndiv"]["total"], 
          testResults["linesIndiv"]["correct"] / testResults["linesIndiv"]["total"] * 100,
          binom_test(round(testResults["linesIndiv"]["correct"]), 
                     testResults["linesIndiv"]["total"], 
                     p=majorityProportion)))

#        print("confusionMap: {}".format(testResults["linesIndiv"]["confusionMap"]))
#        dll.print_f_scores(testResults["linesIndiv"]["confusionMap"])
        #for i in range(NUM_CLASSES):

    print(color("finished: {}".format(datetime.datetime.now()), fg='green'))


if __name__ == "__main__":
    main()
