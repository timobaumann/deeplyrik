#!/usr/bin/env python3
import argparse
import nltk.translate.bleu_score, nltk.tokenize.nist
import os

parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('--data_path_r', required=True, type=str, default='')
parser.add_argument('--data_path_h', required=True, type=str, default='')
parser.add_argument('--poem_suffix_r', type=str, default='/txt.lab')
parser.add_argument('--poem_suffix_h', type=str, default='/txt.lab')
parser.add_argument('--poem_format_r', type=str, default='txt-headerless')
parser.add_argument('--poem_format_h', type=str, default='txt-headerless')

args = parser.parse_args()
print(args)

import deeplyriklib as dll
dll.args = args


nist = nltk.tokenize.nist.NISTTokenizer()

def cleanAndTokenize(line):
    line = line.replace('u\xa0', u' ').strip()
    tokens = nist.tokenize(line, lowercase=True)
    return tokens

def loadCorpus(x='r'):
    args.data_path = getattr(args, "data_path_{}".format(x))
    args.poem_format = getattr(args, "poem_format_{}".format(x))
    setattr(args, "poem_{}_suffix".format(args.poem_format), getattr(args, "poem_suffix_{}".format(x)))
    lines = []
    for subdir in sorted(os.listdir(args.data_path)):
        p = dll.Poem(subdir, args.data_path, args.poem_format)
        plines = [cleanAndTokenize(l) for l in p.getLines()]
        plines = [inner for outer in plines for inner in outer]
        lines.append(plines)
    return lines


ref = loadCorpus('r')
hyp = loadCorpus('h')
assert len(ref) == len(hyp) # same number of poems
flatref = []
flathyp = []
for r,h in zip(ref, hyp):
    flatref.append([r])
    flathyp.append(h)
#    if len(r) == len(h):
#        for rl, hl in zip(r, h):
#            if rl != [] and hl != []:
#                flatref.append([rl])
#                flathyp.append(hl)
#    else:
#        print("warning for: \n", r, h) # different number of lines

#print(len(flatref))

#for r,h in zip(flatref, flathyp):
#    print(r)
#    print(h)
#    print(nltk.translate.bleu_score.sentence_bleu(r, h))

print(nltk.translate.bleu_score.corpus_bleu(flatref, flathyp))
#print(ref)
#print(hyp)
