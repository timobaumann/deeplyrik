#!/usr/bin/env python3
import warnings
import dynet_config
dynet_config.set(random_seed=0, autobatch=1)
import dynet as dy
import random
import sys
sys.path.append('../')
import deeplyriklib as dll

char_embed_size = 20
line_seq_layers = 2
line_seq_hidden_size = 20
line_attention_size = 20
dropout = 0.2

# goal: train encodings for: character embeddings, line-rnns, attention that recreate the line
# intent: pre-training for the line-input in our poems based on text input (poems or similar stuff)
# creates a file called "pretraining.dypar" that contains all trained parameters
# usage: ./pretrain.py  (no need for arguments at the moment)
# The input files are read as shown below [(lyrischespretrainingmaterial.txt), (lyriklinekomplett.txt), (syllLettr.txt)]
# The output is: (char_embeddings.dypar) and (pretraining.dypar)

with open('lyrischespretrainingmaterial.txt') as input:
    sentences = [x.rstrip() for x in input.readlines() if x != "\n"]
with open('lyriklinekomplett.txt') as input:
    sentences.extend([x.rstrip() for x in input.readlines() if x != "\n"])
with open('syllLettr.txt') as input:
    sentences.extend([x.rstrip() for x in input.readlines() if x != "\n"])
print('INFO: total sentences: {}'.format(len(sentences)))
characters = set()
for sentence in sentences:
    characters = characters.union(set(sentence))
characters = list(sorted(list(characters)))
characters.append('<EOL>')

pc = dy.ParameterCollection()
params = {}


print(characters)
params["int2char"] = list(characters)
params["char2int"] = {c:i for i,c in enumerate(characters)}

VOCAB_SIZE = len(characters)
INPUT_DIM = char_embed_size

#params["char_lookup"] = pc.load_lookup_param('char_embeddings.dypar', 'character_embeddings')
params["char_lookup"] = pc.add_lookup_parameters((VOCAB_SIZE, char_embed_size), name='charLookup')

params["fwdCharLSTM"] = dy.GRUBuilder(line_seq_layers, char_embed_size, line_seq_hidden_size, pc)
params["bwdCharLSTM"] = dy.GRUBuilder(line_seq_layers, char_embed_size, line_seq_hidden_size, pc)
params["charAtt_MLP_W"] = pc.add_parameters((line_attention_size, line_seq_hidden_size * 2), name='charAttMLPW') # fwd&bwd
params["charAtt_MLP_bias"] = pc.add_parameters((line_attention_size), name='charAttMLPbias')
params["charAtt_U"] = pc.add_parameters((1, line_attention_size), name='charAttU')

# idea: the decoder gets the line-vector as initial state and receives characters (previous ideal output)
# and generates a state that is turned into the output prediction via W and b.
params["fwdDecLSTM"] = dy.GRUBuilder(1, char_embed_size, line_seq_hidden_size*2, pc)
params["bwdDecLSTM"] = dy.GRUBuilder(1, char_embed_size, line_seq_hidden_size*2, pc)
params["fwdDec_W"] = pc.add_parameters((VOCAB_SIZE, line_seq_hidden_size*2), name='fwdDecW')
params["fwdDec_b"] = pc.add_parameters((VOCAB_SIZE), name='fwdDecb')
params["bwdDec_W"] = pc.add_parameters((VOCAB_SIZE, line_seq_hidden_size*2), name='bwdDecW')
params["bwdDec_b"] = pc.add_parameters((VOCAB_SIZE), name='bwdDecb')


def autoencoding_loss(inputText, params, isTrain=True):
    dy.renew_cg()
    line_output = dll.compute_line_network_output(inputText, None, None, params, isTrain)
    loss = []
    loss.extend(decode("fwdDec", line_output, inputText, params))
    loss.extend(decode("bwdDec", line_output, list(reversed(inputText)), params))
    return dy.esum(loss)

def decode(rnnName, inputState, inputText, params, isTrain=True):
    loss = [] # one loss per character in sentence
    sentence = ['<EOL>'] + list(inputText)  + ['<EOL>']
    sentence = [params["char2int"][c] for c in sentence]
    rnn = params[rnnName+"LSTM"]
    if isTrain:
        rnn.set_dropout(dropout)
    W = dy.parameter(params[rnnName+"_W"])
    b = dy.parameter(params[rnnName+"_b"])
    lookup = params["char_lookup"]
    s = rnn.initial_state(dy.transpose(inputState))
    for char,next_char in zip(sentence,sentence[1:]):
        s = s.add_input(lookup[char])
        probs = dy.softmax(W*s.output() + b)
        loss.append(-dy.log(dy.pick(probs, next_char)))
    return loss

def freeDecode(rnnName, inputText, params):
    dy.renew_cg()
    line_output = dll.compute_line_network_output(inputText, None, None, params)
    rnn = params[rnnName+"LSTM"]
    W = dy.parameter(params[rnnName+"_W"])
    b = dy.parameter(params[rnnName+"_b"])
    lookup = params["char_lookup"]
    s = rnn.initial_state(dy.transpose(line_output))
    s = s.add_input(lookup[params["char2int"]["<EOL>"]])
    out = []
    while True:
        probs = dy.softmax(W*s.output() + b)
        probs = probs.vec_value()
        next_char = sample(probs)
        out.append(params["int2char"][next_char])
        if out[-1] == "<EOL>": break
        s = s.add_input(lookup[next_char])
    return "".join(out[:-1])

def sample(probs):
    rnd = random.random()
    for i,p in enumerate(probs):
        rnd -= p
        if rnd <= 0: break
    return i

def multipletrain(rnn, sentences):
    trainer = dy.AdamTrainer(pc)
    i = 1
    aggrloss = 0
    all_loss = 0
    for sentence in sentences:
        #loss = do_one_sentence(rnn, sentence)
        loss = autoencoding_loss(sentence, params)
        loss_value = loss.value()
        if loss_value <= 1e+10:
            loss.backward()
            try:
                trainer.update()
            except RuntimeError as err:
                print(err)
                print("ERRR: problem with trainer.update(). Reset gradient and moving on.")
                sys.exit() # TODO: find a better way to deal with this
            aggrloss += loss_value
            all_loss += loss_value
        else:
            print("WARN: skipping huge loss {}".format(loss_value))
        if i % 40000 == 0:
            print("DEBG:", i, aggrloss*.000025)#, generate(rnn))
            testSent = "Dies ist ein Satz und Du machst Platz."
            print("INFO:", testSent, autoencoding_loss(testSent, params, isTrain=False).value())
            print("INFO: fwd", freeDecode("fwdDec", testSent, params))
            print("INFO: bwd", "".join(reversed(freeDecode("bwdDec", testSent, params))))
            aggrloss = 0
        i += 1
    print("INFO: loss for full iteration: {}".format(all_loss/i))

if __name__ == "__main__":
    for i in range(1,30):
        random.shuffle(sentences)
        multipletrain(None, sentences)
        params["char_lookup"].save('char_embeddings.dypar', key='character_embeddings')
        pc.save('pretraining.dypar')
#    dy.save(
