#!/usr/bin/env python3
# write a label file that contains labels that span inbetween existing labels.
# the purpose is to get all silences from inbetween voice-stretches.
# leading and trailing silences (before/after the labels) are discarded
# resulting labels may have zero-duration
import codecs
import sys

with codecs.open(sys.argv[1], 'r', encoding='utf-8', errors='ignore') as f:
    labLines = []
    for line in f.readlines():
        line = line.rstrip()
        fields = line.split(maxsplit=2)
        labLines.append({'start': fields[0], 'end': fields[1], 'label': fields[2]})

#outLines = []
prevEnd = labLines[0]['end']
for labLine in labLines[1:]:
    print("{} {} silence".format(prevEnd, labLine['start']))
    prevEnd = labLine['end']