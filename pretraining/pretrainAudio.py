#!/usr/bin/env python3
import warnings
import dynet_config
dynet_config.set(random_seed=0, autobatch=1)
import dynet as dy
import random
import sys
sys.path.append('../')
import deeplyriklib as dll
import numpy as np

audio_seq_layers = 2
audio_seq_hidden_size = 30
audio_attention_size = 25
# for pauses:
#audio_seq_layers = 1
#audio_seq_hidden_size = 20
#audio_attention_size = 15

decoder_seq_hidden_size = audio_seq_hidden_size * 2

AUDIO_DIM = 2 * (13 + 7) # 13 mfcc, 7 ffv, means and stddev of each

dropout = dll.args.dropout
#dll.args.text_path = '../lyrikline/gedichte/'
dll.args.text_path = '../../../gedichte/'
dll.args.store_pickle = True
#dll.args.poem_lab_suffix = '/vad.lab'
dll.args.poem_lab_suffix = '/nonvad.lab'


(poems, _) = dll.loadFromFile(sys.argv[1], poemFormat='lab')
poems = [x[1] for x in dll.unfoldClassData(poems)]

# goal: train encodings for: audio-line-rnns, attention that recreates the line audio features
# intent: pre-training for the line-input in our poems based on audio input (poems or similar stuff)
# creates a file called "audioPretraining.dypar" that contains all trained parameters by using the "vad" features (you need the line (dll.args.poem_lab_suffix = '/vad.lab'))
# or
# creates a file called "silencePretraining.dypar" by using the "nonvad" features (you need the line (dll.args.poem_lab_suffix = '/nonvad.lab'))
# usage: ./pretrainAudio.py input_poems_file
# usage: ./pretrainAudio.py lyrikline-vad.lab   # The output is (audioPretraining.dypar)
# usage: ./pretrainAudio.py lyrikline-nonvad.lab # The output is (silencePretraining.dypar)

# input_poems_file should contain any\tinput_poem lines, the file to be read must be in the poetry-text format (title,blank line,poem) OR should I directly use SWC?

pc = dy.ParameterCollection()
params = {}

params["fwdAudioLSTM"] = dy.GRUBuilder(audio_seq_layers, AUDIO_DIM, audio_seq_hidden_size, pc)
params["bwdAudioLSTM"] = dy.GRUBuilder(audio_seq_layers, AUDIO_DIM, audio_seq_hidden_size, pc)
params["audioAtt_MLP_W"] = pc.add_parameters((audio_attention_size, audio_seq_hidden_size * 2), name='audioAttMLPW')
params["audioAtt_MLP_bias"] = pc.add_parameters((audio_attention_size), name='audioAttMLPbias')
params["audioAtt_U"] = pc.add_parameters((1, audio_attention_size), name='audioAttU')

# idea: the decoder gets the line-vector as initial state and receives characters (previous ideal output)
# and generates a state that is turned into the output prediction via W and b.
params["fwdDecLSTM"] = dy.GRUBuilder(1, AUDIO_DIM, decoder_seq_hidden_size, pc)
params["bwdDecLSTM"] = dy.GRUBuilder(1, AUDIO_DIM, decoder_seq_hidden_size, pc)
params["fwdDec_W"] = pc.add_parameters((AUDIO_DIM, decoder_seq_hidden_size), name='fwdDecW')
params["fwdDec_b"] = pc.add_parameters((AUDIO_DIM), name='fwdDecb')
params["bwdDec_W"] = pc.add_parameters((AUDIO_DIM, decoder_seq_hidden_size), name='bwdDecW')
params["bwdDec_b"] = pc.add_parameters((AUDIO_DIM), name='bwdDecb')
# these parameters are meant to re-create the duration of the sequence from the encoding
params["fwdDec_Length_W"] = pc.add_parameters((1, decoder_seq_hidden_size), name='fwdDecLengthW')
params["fwdDec_Length_b"] = pc.add_parameters((1), name='fwdDecLengthb')
params["bwdDec_Length_W"] = pc.add_parameters((1, decoder_seq_hidden_size), name='bwdDecLengthW')
params["bwdDec_Length_b"] = pc.add_parameters((1), name='bwdDecLengthb')

def autoencoding_loss(inputLine, params, isTrain=True, avgLineLength=1):
    #line_output = dll.compute_line_network_output(None, inputLine[1], params, isTrain)
    line_output = dll.compute_line_network_output(None, inputLine[1], None, params, isTrain)
    loss = []
    audioVectors = [dy.inputVector(v) for v in inputLine[1]]
    loss.extend(decode("fwdDec", line_output, audioVectors, params, avgLineLength=avgLineLength))
    loss.extend(decode("bwdDec", line_output, list(reversed(audioVectors)), params, avgLineLength=avgLineLength))
    return dy.esum(loss)

def decode(rnnName, inputState, targetVectors, params, isTrain=True, avgLineLength=1):
    loss = [] # one loss per character in sentence
    targetVectors = [dy.zeros(AUDIO_DIM)] + targetVectors + [dy.zeros(AUDIO_DIM)]
    rnn = params[rnnName+"LSTM"]
    if isTrain:
        rnn.set_dropout(dropout)
    W = dy.parameter(params[rnnName+"_W"])
    b = dy.parameter(params[rnnName+"_b"])
    s = rnn.initial_state(dy.transpose(inputState))
    for targetVector,nextTargetVector in zip(targetVectors,targetVectors[1:]):
        s = s.add_input(targetVector)
        loss.append(dy.sqrt(dy.squared_distance(W*s.output()+b, nextTargetVector)))
    lengthW = dy.parameter(params[rnnName+"_Length_W"])
    lengthb = dy.parameter(params[rnnName + "_Length_b"])
    lengthLoss = dy.sqrt(dy.squared_distance(lengthW*inputState+lengthb, dy.inputVector([len(targetVectors)])))
    return dy.esum(loss) / len(targetVectors)  + lengthLoss/avgLineLength

def sample(probs):
    rnd = random.random()
    for i,p in enumerate(probs):
        rnd -= p
        if rnd <= 0: break
    return i

def multipletrain(poems):
    trainer = dy.AdamTrainer(pc)
    i = 0
    batch_i = 0
    all_loss = 0
    accu_loss = 0
    lines = []
    for poem in poems:
        lines.extend(poem.getLinesAndAudio())
    avgLineLength = np.mean([len(l[1]) for l in lines])
    print(avgLineLength)
    random.shuffle(lines)
    batch_size = 1 # it turns out that batching hurts performance (quite badly, actually)
    batches = [lines[i:i+batch_size] for i in range(0, len(lines), batch_size)]
    for batch in batches:
        dy.renew_cg()
        losses = []
        for line in batch:
            loss = autoencoding_loss(line, params, avgLineLength=avgLineLength)
            losses.append(loss)
            i += 1
            batch_i += 1
        batch_loss = dy.esum(losses) / batch_size
        batch_loss_value = batch_loss.value()
        if batch_loss_value <= 1e+10:
            batch_loss.backward()
            try:
                trainer.update()
            except RuntimeError as err:
                print(err)
                print("ERRR: problem with trainer.update(). Reset gradient and moving on.")
                pc.save('silencePretraining.dypar-')
                #sys.exit() # TODO: find a better way to deal with this
                pc.resetGradient()
            all_loss += batch_loss_value * batch_size
            accu_loss += batch_loss_value * batch_size
        else:
            print("WARN: skipping huge loss {}".format(batch_loss_value))
        if batch_i >= 1000:
            print("DEBG:{:6}\t{:9.3f}\t{:9.3f}".format(i, accu_loss / batch_i, all_loss / i))
            batch_i = 0
            accu_loss = 0
#            testSent = "Dies ist ein Satz und Du machst Platz."
#            print("INFO:", testSent, autoencoding_loss(testSent, params, isTrain=False).value())
#            print("INFO: fwd", freeDecode("fwdDec", testSent, params))
#            print("INFO: bwd", "".join(reversed(freeDecode("bwdDec", testSent, params))))
    print("INFO: loss for full iteration: {}".format(all_loss/len(lines)))

if __name__ == "__main__":
    for i in range(1,30):
#        random.shuffle(poems) # shuffling from within train
        multipletrain(poems)
        #pc.save('audioPretraining.dypar')  # create the "audioPretraining.dypar" by using the "vad" features (you need the line (dll.args.poem_lab_suffix = '/vad.lab'))
        pc.save('silencePretraining.dypar') # create the "silencePretraining.dypar" by using the "nonvad" features (you need the line (dll.args.poem_lab_suffix = '/nonvad.lab'))
#    dy.save(
