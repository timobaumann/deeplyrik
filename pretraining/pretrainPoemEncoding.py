#!/usr/bin/env python3
import warnings
import dynet_config
dynet_config.set(random_seed=0, autobatch=1)
import dynet as dy
import random
import sys
sys.path.append('../')
import deeplyriklib as dll

char_embed_size = 20
line_seq_layers = 2
line_seq_hidden_size = 20
line_attention_size = 20
poem_seq_layers = 2
poem_seq_hidden_size = 20
poem_attention_size = 20
dropout = 0.2

dll.args.normalize_acoustics = False

decoder_seq_hidden_size = poem_seq_hidden_size * 2

# goal: given some (pre-computed) line-encodings, compute poem encodings to replicate the line-encodings (as best as possible)
# stops back-propagation before the original line-encoding parameters would be changed: dynet.nobackprop(X) for every line-output X
# (this seems like the right thing to do, in order to avoid creating line-encodings that adapt to their replicability)

# usage: ./pretrainPoemEncoding.py input_poems_file

# input_poems_file should contain class\tinput_poem lines, the file to be read must be in the poetry-text format (title,blank line,poem) OR should I directly use SWC?

(poems, _) = dll.loadFromFile(sys.argv[1], poemFormat='txt')
poems = [x[1] for x in dll.unfoldClassData(poems)]

pc = dy.ParameterCollection()
params = {}

CHAR_TYPES = ['\t', ' ', '!', '"', '#', '$', '%', '&', "'", '(', ')', '*', '+', ',', '-', '.', '/', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ':', ';', '<', '=', '>', '?', '@', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '[', ']', '^', '_', '`', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '|', '~', '©', '«', '¬', '\xad', '¯', '°', '´', '·', 'º', '»', 'Ä', 'Ç', 'É', 'Ó', 'Ö', '×', 'Ø', 'Ü', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'ö', 'ø', 'ú', 'û', 'ü', 'ā', 'ą', 'ē', 'ı', 'ł', 'ŉ', 'ŋ', 'š', 'ţ', 'ū', 'ż', 'ſ', 'ɘ', '̈', 'Α', 'Γ', 'Θ', 'Ι', 'Κ', 'Ν', 'Ο', 'Σ', 'Τ', 'Υ', 'Ω', 'α', 'β', 'γ', 'δ', 'ε', 'η', 'ι', 'λ', 'μ', 'ν', 'ο', 'π', 'σ', 'τ', 'ω', 'ϑ', 'ϱ', 'Б', 'В', 'Н', 'С', 'Ы', 'Э', 'а', 'в', 'г', 'д', 'е', 'ж', 'и', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'х', 'ц', 'ш', 'щ', 'ы', 'ь', 'э', 'أ', 'ا', 'ة', 'ر', 'ش', 'ض', 'ف', 'ἐ', 'Ἔ', 'ἣ', 'ὅ', 'Ὦ', 'ὴ', 'ώ', 'ῆ', 'ῖ', '‒', '–', '—', '‘', '’', '‚', '“', '”', '„', '†', '•', '…', '‹', '›', '№', '→', '−', '∙', '√', '│', '◊', '・', 'ﬂ', '<EOL>', 'č', "✴", "}",  "ˆ", "{", "²", "¸", ""]
INPUT_DIM = len(CHAR_TYPES)

params["int2char"] = list(CHAR_TYPES)
params["char2int"] = {c:i for i,c in enumerate(CHAR_TYPES)}
params["char_lookup"] = pc.load_lookup_param('pretraining.dypar', '/charLookup')

params["fwdCharLSTM"] = dy.GRUBuilder(line_seq_layers, char_embed_size, line_seq_hidden_size, pc)
params["fwdCharLSTM"].param_collection().populate('pretraining.dypar', '/gru-builder/')
params["bwdCharLSTM"] = dy.GRUBuilder(line_seq_layers, char_embed_size, line_seq_hidden_size, pc)
params["bwdCharLSTM"].param_collection().populate('pretraining.dypar', '/gru-builder_1/')
params["charAtt_MLP_W"] = pc.load_param('pretraining.dypar', '/charAttMLPW')
params["charAtt_MLP_bias"] = pc.load_param('pretraining.dypar', '/charAttMLPbias')
params["charAtt_U"] = pc.load_param('pretraining.dypar', '/charAttU')
# per-poem representation of line-outputs
lineDecLayerInputDims = line_seq_hidden_size * 2
params["fwdLineLSTM"] = dy.GRUBuilder(poem_seq_layers, lineDecLayerInputDims, poem_seq_hidden_size, pc)
params["bwdLineLSTM"] = dy.GRUBuilder(poem_seq_layers, lineDecLayerInputDims, poem_seq_hidden_size, pc)
params["lineAtt_MLP_W"] = pc.add_parameters((poem_attention_size, poem_seq_hidden_size * 2), name='lineAttMLPW')
params["lineAtt_MLP_bias"] = pc.add_parameters((poem_attention_size), name='lineAttMLPbias')
params["lineAtt_U"] = pc.add_parameters((1, poem_attention_size), name='lineAttMLPU')

# idea: the decoder gets the line-vector as initial state and receives characters (previous ideal output)
# and generates a state that is turned into the output prediction via W and b.
params["fwdDecLSTM"] = dy.GRUBuilder(1, line_seq_hidden_size*2, decoder_seq_hidden_size, pc)
params["bwdDecLSTM"] = dy.GRUBuilder(1, line_seq_hidden_size*2, decoder_seq_hidden_size, pc)
# our goal will be to recreate the (attended) line-RNN output vectors 
params["fwdDec_W"] = pc.add_parameters((line_seq_hidden_size*2, decoder_seq_hidden_size), name='fwdDecW')
params["fwdDec_b"] = pc.add_parameters((line_seq_hidden_size*2), name='fwdDecb')
params["bwdDec_W"] = pc.add_parameters((line_seq_hidden_size*2, decoder_seq_hidden_size), name='bwdDecW')
params["bwdDec_b"] = pc.add_parameters((line_seq_hidden_size*2), name='bwdDecb')

def compute_poem_network_output(poem, params, isTrain=False, attentionStore=None):
    dy.renew_cg()
    line_outputs = [dy.inputVector([0.0 for x in range(line_seq_hidden_size * 2)])]
    lines = [l for l in poem.getLines() if l != ""]        
    for line in lines:
        line_output = dll.compute_line_network_output(line, None, None, params, isTrain)
        line_output = dy.nobackprop(line_output) # avoid changing the line outputs
        line_outputs.append(line_output)
    line_outputs.append(dy.inputVector([0.0 for x in range(line_seq_hidden_size * 2)]))
    fwd_poem_output = dll.run_rnn(params, "fwdLineLSTM", line_outputs, isTrain)
    bwd_poem_output = dll.run_rnn(params, "bwdLineLSTM", list(reversed(line_outputs)), isTrain)
    poem_output = dll.aggregateSeqResults(fwd_poem_output, bwd_poem_output, params, "line", attentionStore)
    return (poem_output, line_outputs)

def autoencoding_loss(poem, params, isTrain=True):
    dy.renew_cg()
    (poem_output, line_outputs) = compute_poem_network_output(poem, params, isTrain)
    loss = []
    loss.extend(decode("fwdDec", poem_output, line_outputs, params))
    loss.extend(decode("bwdDec", poem_output, list(reversed(line_outputs)), params))
    return dy.esum(loss)

def decode(rnnName, inputState, targetVectors, params, isTrain=True):
    loss = [] # one loss per character in sentence
    rnn = params[rnnName+"LSTM"]
    if isTrain:
        rnn.set_dropout(dropout)
    W = dy.parameter(params[rnnName+"_W"])
    b = dy.parameter(params[rnnName+"_b"])
    s = rnn.initial_state(dy.transpose(inputState))
    for targetVector,nextTargetVector in zip(targetVectors,targetVectors[1:]):
        s = s.add_input(targetVector)
        loss.append(dy.sqrt(dy.squared_distance(W*s.output()+b, nextTargetVector)))
    return loss

def multipletrain(poems):
    trainer = dy.AdamTrainer(pc)
    i = 1
    aggrloss = 0
    all_loss = 0
    for poem in poems:
        #print("{}: ''{}''".format(poem.fileFor(''), poem.getAllText()))#getShortDescription()))
        loss = autoencoding_loss(poem, params)
        loss_value = loss.value()
        if loss_value <= 1e+10:
            loss.backward()
            try:
                trainer.update()
            except RuntimeError as err:
                print(err)
                print("ERRR: problem with trainer.update(). Reset gradient and moving on.")
                sys.exit() # TODO: find a better way to deal with this
            aggrloss += loss_value
            all_loss += loss_value
        else:
            print("WARN: skipping huge loss {}".format(loss_value))
#        if i % 400 == 0:
#            print("DEBG:", i, aggrloss*.0025)
#            testSent = "Dies ist ein Satz und Du machst Platz."
#            print("INFO:", testSent, autoencoding_loss(testSent, params, isTrain=False).value())
#            print("INFO: fwd", freeDecode("fwdDec", testSent, params))
#            print("INFO: bwd", "".join(reversed(freeDecode("bwdDec", testSent, params))))
#            aggrloss = 0
        i += 1
    print("INFO: loss for full iteration: {}".format(all_loss/i))

if __name__ == '__main__':
    for i in range(1,30):
        random.shuffle(poems)
        multipletrain(poems)
        pc.save('poempretraining.dypar')


