#DeepLyrik -- poetic style classification for read-out (post-modern) poetry in the Rhythmicalizer project

See also http://rhythmicalizer.net

The code is used in the experiments for the papers:

```bibtex
@INPROCEEDINGS { poetry2018coling,
    AUTHOR = { Timo Baumann and Hussein Hussein and Burkhard Meyer-Sickendiek },
    TITLE = { Style Detection for Free Verse Poetry from Text and Speech },
    BOOKTITLE = { Proceedings of COLING },
    YEAR = { 2018 },
    ADDRESS = { Santa Fe, USA },
    MONTH = { aug },
    SORTYEAR = { 2018-08 },
    PAGES = { 1929-1940 },
    URL = { https://aclanthology.info/papers/C18-1164/c18-1164 }
}

@INPROCEEDINGS { poetry2018latech,
    AUTHOR = { Timo Baumann and Hussein Hussein and Burkhard },
    TITLE = { Analysis of Rhythmic Phrasing: Feature Engineering vs. Representation Learning for Classifying Readout Poetry },
    BOOKTITLE = { Proceedings of the Joint LaTeCH&CLfL Workshop },
    YEAR = { 2018 },
    ORGANIZATION = { Association for Computational Linguistics },
    PAGES = { 44-49 },
    MONTH = { sep },
    ADDRESS = { Santa Fe, USA },
    SORTYEAR = { 2018-09-1 },
    URL = { https://aclanthology.info/papers/W18-4505/w18-4505 }
} 

@INPROCEEDINGS { poetry2018interspeech,
    AUTHOR = { Timo Baumann and Hussein Hussein and Burkhard Meyer-Sickendiek },
    TITLE = { Analysing the Focus of a Hierarchical Attention Network: The Importance of Enjambments When Classifying Post-modern Poetry },
    BOOKTITLE = { Proceedings of Interspeech },
    YEAR = { 2018 },
    ADDRESS = { Hyderabad, India },
    MONTH = { sep },
    SORTYEAR = { 2018-09-5 },
    DOI = { 10.21437/Interspeech.2018-2530 },
} 
```
