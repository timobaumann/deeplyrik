#!/usr/bin/env python3
# manual annotation aligner / alignment checker
# usage: alignLabs.py labfile txtfile
import codecs
from colors import color
from brew_distance import distance
import argparse

parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('--txt', type=str,
                    help='text as it falls out of our automatic lyrikline extraction: title-lines, empty line, poem lines (with empty lines)')
parser.add_argument('--lab', type=str,
                    help='alignment file with either the poem text or other labels (that should then relate to mantxt lines')
parser.add_argument('--txtlab', type=str)
parser.add_argument('--mantxt', type=str, default=None,
                    help='manually corrected version of text, hopefully with identical lines as in alignment file')
parser.add_argument('--outlab', type=str, default=None,
                    help='where to put the merged label output ')
args = parser.parse_args()
print(args)

with codecs.open(args.lab, 'r', encoding='utf-8', errors='ignore') as f:
    labLines = []
    for line in f.readlines():
        line = line.rstrip()
        if (line == "" or line == "title"):
            labLines.append({'start' : 'N/A', 'end' : 'N/A', 'label': ''})
        else:
            fields = line.split(maxsplit=2)
            labLines.append({'start': fields[0], 'end': fields[1], 'label': fields[2]})
if args.mantxt != None:
    with codecs.open(args.mantxt, 'r', encoding='utf-8', errors='ignore') as f:
        manTxtLines = [l.rstrip() for l in f.readlines()]# if l.strip() != ""]
    assert len(labLines) == len(manTxtLines), "different length of alignment and aligned text: " + str(len(labLines)) + " vs. " + str(len(manTxtLines))
    for (labLine, manTxt) in zip(labLines, manTxtLines):
        labLine['label'] = manTxt
#print("\n".join([l['start']+" "+l['end']+" "+l['label'] for l in labLines]))

# if args.txtlab != None:
#     # make up timings in labLines:
#     #
#     lastKnownTime = 0.01
#     for l in labLines:
#         if (l['start'] == 'N/A'):
#             l['start'] = str(lastKnownTime + 0.3)
#             l['end'] = str(lastKnownTime + 0.4)
#             lastKnownTime += 0.7
#         else:
#             lastKnownTime = float(l['end'])
#     with open(args.txtlab, 'w') as f:
#         f.write('\n'.join([l['start']+" "+l['end']+" "+l['label'] for l in labLines if l['label'] != '']))
#         f.write('\n')

with open(args.txt) as f:
    title = f.readline()
    while title != "":
        title = f.readline().rstrip()
    txtLines = [l.rstrip() for l in f.readlines()]
#print("\n".join(txtLines))
labText = [l['label'] for l in labLines]
(cost, edits) = distance(labText, txtLines, "both", cost=(0, 1, 1, 1.2), flexibleSubst=True)
#print(cost)
#print(edits)
i = 0
j = 0
outLines = []
for edit in edits:
    if edit == 'MATCH':
        print("{:70}{}".format(labText[i], txtLines[j]))
        if (labLines[i]['start'] != 'N/A'):
            outLines.append("{} {} {}".format(labLines[i]['start'], labLines[i]['end'], txtLines[j]))
        i += 1
        j += 1
    elif edit == 'SUBST':
        print(color("{:70}{}".format(labText[i], txtLines[j]), fg="orange"))
        if (labLines[i]['start'] != 'N/A'):
            outLines.append("{} {} {}".format(labLines[i]['start'], labLines[i]['end'], txtLines[j]))
        i += 1
        j += 1
    elif edit == 'INS':
        print(color("{:70}{}".format("", txtLines[j]), fg="blue"))
        j += 1
    elif edit == 'DEL':
        print(color(labText[i], fg="red"))
        i += 1
if args.outlab != None:
    with open(args.outlab, 'w') as f:
        f.write('\n'.join(outLines))
        f.write('\n')
else:
    print('\n'.join(outLines))
