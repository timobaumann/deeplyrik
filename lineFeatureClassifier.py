#!/usr/bin/env python3
import argparse

parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('--random_seed', type=int, default=1, help='set dynet and python random seeds for replicability; 0 implies truly random seed')
parser.add_argument('--no_sleep', action='store_true', help='switch off random startup sleep (useful on NFS-based clusters for multi-processing)')
parser.add_argument('--char_embed_size', type=int, default=20)
parser.add_argument('--line_seq_layers', type=int, default=2)
parser.add_argument('--line_seq_hidden_size', type=int, default=20)
# TODO: Option um attention vs. final output (potentially more possibilities (mean, max, ...))
parser.add_argument('--line_attention_size', type=int, default=20)
parser.add_argument('--ignore_audio', action='store_true')
parser.add_argument('--ignore_pause', action='store_true')
parser.add_argument('--ignore_text', action='store_true')
parser.add_argument('--no_pretraining', action='store_true')
parser.add_argument('--normalize_acoustics', type=bool, default=True)
parser.add_argument('--audio_seq_step', type=int, default=10, help='how many feature frames to combine; will be overridden by pickle\'d value') # 100ms
parser.add_argument('--audio_seq_layers', type=int, default=2)
parser.add_argument('--audio_seq_hidden_size', type=int, default=30)
parser.add_argument('--audio_attention_size', type=int, default=25)
parser.add_argument('--pause_seq_layers', type=int, default=1)
parser.add_argument('--pause_seq_hidden_size', type=int, default=20)
parser.add_argument('--pause_attention_size', type=int, default=15)
parser.add_argument('--line_annotation_suffix', type=str, default='/txt.lab.anno', help='file suffix for line-by-line annotations')
parser.add_argument('--dropout', type=float, default=0.2, help='dropout probability (applied on all layers)')
parser.add_argument('--iterations', type=int, default=30, help='training iterations for the network; use fewer iterations without dropout')
#parser.add_argument('--test_size', type=int, default=1, help='size of each test set during cross-fold validation')
parser.add_argument('--test_folds', type=int, default=10, help='number of folds during cross-fold validation')
parser.add_argument('--data_path', type=str, default='', help='path to data files; UNUSED if using pickled data') # /home/timo/Dropbox/Prosodische_Muster_Gedicht/
parser.add_argument('--trials', type=int, default=1, help='the number of times to run classification for each file to balance out effects from random initialization')
parser.add_argument('--store_pickle', default=True, action='store_true', help='store a pickle of the loaded plain text input poem file')
parser.add_argument('--dynet_autobatch', type=int, default=1)
parser.add_argument('--line_feature', type=str, required=True, help='the line feature to train/test on')
parser.add_argument('--line_feature_values', type=str, default='j;n', help='classes of the line feature, separated by semicolon')
parser.add_argument('input_poems_file', nargs='+', help='specification of poems and assigned classes; either plain text (see README) or pickle (if suffix is .pickle)')
args = parser.parse_args()
args.poem_lab_suffix = '/txt.lab'
args.ignore_anno = False
print(args)

import dynet_config
dynet_config.set(random_seed=args.random_seed, autobatch=args.dynet_autobatch, mem=1536)
import dynet as dy
import datetime
from random import shuffle, random, seed
if args.random_seed != 0:
    seed(args.random_seed)
from scipy.stats import binom_test
from time import sleep
from colors import color # color on the commandline!, corresponding module is called "ansicolors" in pip
import deeplyriklib as dll
import numpy as np

AUDIO_DIM = 2 * (13 + 7) # 13 mfcc, 7 ffv, means and stddev of each

args.AUDIO_DIM = AUDIO_DIM
dll.args = args

if not args.no_sleep:
    sleeptime = random() * 8
    print("sleeping for {:.3} s".format(sleeptime))
    sleep(sleeptime)


def trainOnLines(trainLines, trainer, params, iterations):
    for iteration in range(iterations):
        shuffle(trainLines)
        i = 0
        aggrloss = 0
        for (className, line) in trainLines:
            className = line[3][args.line_feature]
            if className != 'u':
                loss = dll.get_line_loss(className, line[0], line[1], line[2], None, params)
                loss_value = loss.value()
                aggrloss += loss_value
                loss.forward()
                loss.backward()
                trainer.update()
                i += 1
        print("per-line iteration:\t{:02},\tavg. loss:\t{:.3}\r".format(iteration, aggrloss/i), end='')
    print() # add newline (above print does not print newlines)


def testOnLines(testLines, params, testResults):
    testCorrect = 0
    for (className, line) in testLines:
        className = line[3][args.line_feature]
        if className != 'u':
            # a "line" contains both text and audio, but not the class label
            att_weights = []
            mlp_output = dll.compute_line_network_decision(line[0], line[1], line[2], params, attentionStore=att_weights)
            output = mlp_output.value()
            best = max([(v,i) for (i,v) in enumerate(output)])
            estimatedClass = best[1]
            estimatedName = params["int2class"][estimatedClass]
            realClass = params["class2int"][className]
            correct = estimatedClass == realClass
            #testResultClasses[params["int2class"][best[1]]] += 1
            testResults["total"] += 1
            testResults["correct"] += 1 if correct else 0
            testCorrect += 1 if correct else 0
            testResults["confusionMap"][realClass, estimatedClass] += 1 #["{}->{}".format(className, params["int2class"][best[1]])] += 1
            loss = dy.pickneglogsoftmax(mlp_output, best[1])
            print("classified as:\t{},\tloss:\t{:5.3f}\t".format(estimatedName, loss.value()), end='')
            colors = dll.colorsForWeights(att_weights)
            for (char, charcolor) in zip(' ' + line[0] + ' ', colors):
                print(color(char, fg='white', bg=charcolor), end='')
            print()
    return testCorrect


def trainAndTest(trainPoems, pc, params, iterations=50, testPoems=[], testResults=[]):
    #print(color("trainAndTest >>>>>>> Start ...", fg='green'))
    trainer = dy.AdamTrainer(pc)
    
    poemsCorrect = 0
    ### unfold training poems into individual lines
    trainLines = unfoldToLineData(trainPoems)
    testLines = unfoldToLineData(testPoems)

    trainOnLines(trainLines, trainer, params, iterations)
    # perform per-line evaluation and print (brief) stats
    linesCorrect = testOnLines(testLines, params, testResults=testResults["linesIndiv"])
    print("lines: {}/{} ({:5.1f}%)".format(linesCorrect, len(testLines), 100.0*linesCorrect/len(testLines)))


def unfoldToLineData(poems):
    # ignore lines without text ("")
    return [(p.getClass(), l) for p in poems for l in p.getLinesAndAudio() if l[0] != ""]


def main():
    """start with one tab-separated-values file as parameter. 
       Each line should contain info for one poem: 
       the poem's class, path to file containing the poem's text"""
    print(color("started: {}".format(datetime.datetime.now()), fg='green'))
    print("loading input from [{}]".format(', '.join(args.input_poems_file)))
    (classData, observed_char_types) = dll.loadFromFiles(args.input_poems_file, poemFormat='lab')
    # classData contains a dict; every class is a key with lists where entries represent poems
    #print(len(classData), NUM_CLASSES, CHAR_TYPES)
    CHAR_TYPES = ['\t', ' ', '!', '"', '#', '$', '%', '&', "'", '(', ')', '*', '+', ',', '-', '.', '/', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ':', ';', '<', '=', '>', '?', '@', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '[', ']', '^', '_', '`', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '|', '~', '©', '«', '¬', '\xad', '¯', '°', '´', '·', 'º', '»', 'Ä', 'Ç', 'É', 'Ó', 'Ö', '×', 'Ø', 'Ü', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'ö', 'ø', 'ú', 'û', 'ü', 'ā', 'ą', 'ē', 'ı', 'ł', 'ŉ', 'ŋ', 'š', 'ţ', 'ū', 'ż', 'ſ', 'ɘ', '̈', 'Α', 'Γ', 'Θ', 'Ι', 'Κ', 'Ν', 'Ο', 'Σ', 'Τ', 'Υ', 'Ω', 'α', 'β', 'γ', 'δ', 'ε', 'η', 'ι', 'λ', 'μ', 'ν', 'ο', 'π', 'σ', 'τ', 'ω', 'ϑ', 'ϱ', 'Б', 'В', 'Н', 'С', 'Ы', 'Э', 'а', 'в', 'г', 'д', 'е', 'ж', 'и', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'х', 'ц', 'ш', 'щ', 'ы', 'ь', 'э', 'أ', 'ا', 'ة', 'ر', 'ش', 'ض', 'ف', 'ἐ', 'Ἔ', 'ἣ', 'ὅ', 'Ὦ', 'ὴ', 'ώ', 'ῆ', 'ῖ', '‒', '–', '—', '‘', '’', '‚', '“', '”', '„', '†', '•', '…', '‹', '›', '№', '→', '−', '∙', '√', '│', '◊', '・', 'ﬂ', '<EOL>']
    for char in observed_char_types:
        assert char in CHAR_TYPES, 'character "{}" in input data cannot be mapped to known char types'.format(char)
    INPUT_DIM = len(CHAR_TYPES)

    params = {}
    params["int2char"] = list(CHAR_TYPES)
    params["char2int"] = {c:i for i,c in enumerate(CHAR_TYPES)}
    params["int2class"] = args.line_feature_values.split(';')
    params["class2int"] = {c:i for i,c in enumerate(params["int2class"])}
    NUM_CLASSES = len(params["int2class"])
    allPoems = [x[1] for x in dll.unfoldClassData(classData)]
    numPoems = len(allPoems)


    allRuns = args.test_folds * args.trials
    runIndex = 0
    testResults = { "linesIndiv" : { "total" : 0, "correct" : 0, "confusionMap" : np.zeros(shape=[NUM_CLASSES, NUM_CLASSES])},
                    "linesGlobal" : { "total" : 0, "correct" : 0, "confusionMap" : np.zeros(shape=[NUM_CLASSES, NUM_CLASSES])},
                    "poems" : { "total" : 0, "correct" : 0, "confusionMap" : np.zeros(shape=[NUM_CLASSES, NUM_CLASSES])}
                  }
    assert args.test_folds <= numPoems, "test set size is too large for the given number of poems: {} vs. {}".format(args.test_folds, numPoems)

    for i in range(args.trials):
        # shuffle poems in each class
        folds = [[] for x in range(args.test_folds)]
        # for every class, shuffle items in class, add items to buckets, shuffle buckets, add buckets to folds
        for (poemClass, poems) in classData.items():
            shuffle(poems)
            classFolds = [[] for x in range(args.test_folds)]
            for i in range(len(poems)):
                classFolds[i % args.test_folds].append(poems[i])
            shuffle(classFolds)
            for i in range(len(folds)):
                folds[i].extend(classFolds[i])
#        print(folds)
        for testPoems in folds:
            runIndex += 1
            print('training run {} of {} ({:5.1f}% done)'.format(runIndex, allRuns, 100*(runIndex-1) / allRuns))

            ### set up dynet anew
            pc = dy.Model()
            # per-line text representation
            params["fwdCharLSTM"] = dy.GRUBuilder(args.line_seq_layers, args.char_embed_size, args.line_seq_hidden_size, pc)
            params["bwdCharLSTM"] = dy.GRUBuilder(args.line_seq_layers, args.char_embed_size, args.line_seq_hidden_size, pc)
            # per-line audio representation
            params["fwdAudioLSTM"] = dy.GRUBuilder(args.audio_seq_layers, AUDIO_DIM, args.audio_seq_hidden_size, pc)
            params["bwdAudioLSTM"] = dy.GRUBuilder(args.audio_seq_layers, AUDIO_DIM, args.audio_seq_hidden_size, pc)
            # between-line audio representation
            params["fwdPauseLSTM"] = dy.GRUBuilder(args.pause_seq_layers, AUDIO_DIM, args.pause_seq_hidden_size, pc)
            params["bwdPauseLSTM"] = dy.GRUBuilder(args.pause_seq_layers, AUDIO_DIM, args.pause_seq_hidden_size, pc)
            if (args.no_pretraining):
                params["char_lookup"] = pc.add_lookup_parameters((INPUT_DIM, args.char_embed_size))
                params["charAtt_MLP_W"] = pc.add_parameters((args.line_attention_size, args.line_seq_hidden_size * 2))
                params["charAtt_MLP_bias"] = pc.add_parameters((args.line_attention_size))
                params["charAtt_U"] = pc.add_parameters((1, args.line_attention_size))
                params["audioAtt_MLP_W"] = pc.add_parameters((args.audio_attention_size, args.audio_seq_hidden_size * 2))
                params["audioAtt_MLP_bias"] = pc.add_parameters((args.audio_attention_size))
                params["audioAtt_U"] = pc.add_parameters((1, args.audio_attention_size))
                params["pauseAtt_MLP_W"] = pc.add_parameters((args.pause_attention_size, args.pause_seq_hidden_size * 2))
                params["pauseAtt_MLP_bias"] = pc.add_parameters((args.pause_attention_size))
                params["pauseAtt_U"] = pc.add_parameters((1, args.pause_attention_size))
            else:
                params["char_lookup"] = pc.load_lookup_param('pretraining/pretraining.dypar', '/charLookup')
                params["fwdCharLSTM"].param_collection().populate('pretraining/pretraining.dypar', '/gru-builder/')
                params["bwdCharLSTM"].param_collection().populate('pretraining/pretraining.dypar', '/gru-builder_1/')
                params["charAtt_MLP_W"] = pc.load_param('pretraining/pretraining.dypar', '/charAttMLPW')
                params["charAtt_MLP_bias"] = pc.load_param('pretraining/pretraining.dypar', '/charAttMLPbias')
                params["charAtt_U"] = pc.load_param('pretraining/pretraining.dypar', '/charAttU')
                params["fwdPauseLSTM"].param_collection().populate('pretraining/silencePretraining.dypar', '/gru-builder/')
                params["bwdPauseLSTM"].param_collection().populate('pretraining/silencePretraining.dypar', '/gru-builder_1/')
                params["fwdAudioLSTM"].param_collection().populate('pretraining/audioPretraining.dypar', '/gru-builder/')
                params["bwdAudioLSTM"].param_collection().populate('pretraining/audioPretraining.dypar', '/gru-builder_1/')
                params["audioAtt_MLP_W"] = pc.load_param('pretraining/audioPretraining.dypar', '/audioAttMLPW')
                params["audioAtt_MLP_bias"] = pc.load_param('pretraining/audioPretraining.dypar', '/audioAttMLPbias')
                params["audioAtt_U"] = pc.load_param('pretraining/audioPretraining.dypar', '/audioAttU')
                params["pauseAtt_MLP_W"] = pc.load_param('pretraining/silencePretraining.dypar', '/audioAttMLPW')
                params["pauseAtt_MLP_bias"] = pc.load_param('pretraining/silencePretraining.dypar', '/audioAttMLPbias')
                params["pauseAtt_U"] = pc.load_param('pretraining/silencePretraining.dypar', '/audioAttU')

            # per-line decision-making (for initial pre-training iteration)
            lineDecLayerInputDims = (args.line_seq_hidden_size * 2
                                  + ((args.audio_seq_hidden_size
                                     + (args.pause_seq_hidden_size if not args.ignore_pause else 0))
                                     * 2 if not args.ignore_audio else 0))
            params["lineDecisionMLP_W"] = pc.add_parameters((NUM_CLASSES, lineDecLayerInputDims)) # Softmax weights
            params["lineDecisionMLP_bias"] = pc.add_parameters((NUM_CLASSES))                 # Softmax bias

            for testPoem in testPoems:
                print("for item {} in class '{}' with text '{}...'".format(testPoem.filenameBase, testPoem.getClass(),
                                                                        testPoem.getShortDescription()))
            trainPoems = []
            for fold in folds:
                if fold != testPoems:
                    trainPoems.extend(fold)
            trainAndTest(trainPoems, pc, params, iterations=args.iterations, testPoems=testPoems, testResults=testResults)
#            print(testResults)
        majorityProportion = max([len(x) for x in classData.values()]) / numPoems
        print(testResults)
        print("{} out of {} correct ({:5.1f}%), p<{:.3}".format(
          testResults["linesIndiv"]["correct"], 
          testResults["linesIndiv"]["total"], 
          testResults["linesIndiv"]["correct"] / testResults["linesIndiv"]["total"] * 100,
          binom_test(round(testResults["linesIndiv"]["correct"]), 
                     testResults["linesIndiv"]["total"], 
                     p=majorityProportion)))

        print("confusionMap: {}".format(testResults["linesIndiv"]["confusionMap"]))
        dll.print_f_scores(testResults["linesIndiv"]["confusionMap"])
        #for i in range(NUM_CLASSES):

    print(color("finished: {}".format(datetime.datetime.now()), fg='green'))


if __name__ == "__main__":
    main()
