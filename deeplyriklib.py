#!/usr/bin/env python3
import sys
from colors import color  # color on the commandline!, corresponding module is called "ansicolors" in pip
import dynet as dy
from collections import defaultdict
import os, re
import itertools as it
import pickle
import numpy as np
import ast

class AttributeDict(dict):
    def __getattr__(self, attr):
        return self[attr]

    def __setattr__(self, attr, value):
        self[attr] = value


args = AttributeDict({
    'dropout': 0.2,
    'audio_seq_step': 10,
    'ignore_audio': False,
    'ignore_pause': False,
    'ignore_text' : False,
    'ignore_anno' : False,
    'store_pickle': False,
    'normalize_acoustics': True,
    'data_path': '',
    'poem_lab_suffix': '/txt.lab',
    'poem_txt_suffix': '/txt',
    'AUDIO_DIM' : 2 * (13 + 7),
})


def smartStdDev(x):
    x = [s for s in x if s != -1.0 and s != -200]
    if not x:  # means empty:
        return -1
    else:
        return np.std(x)


def smartMean(x):
    x = [s for s in x if s != -1.0 and s != -200]
    if not x:  # means empty:
        return -1
    else:
        return np.mean(x)


def grouper(iterable, n):
    args = [iter(iterable)] * n
    return it.zip_longest(fillvalue=0, *args)


def loadFeatStreamFile(filename, subsample=args.audio_seq_step):
    """ return a list of feature vectors as found in each line of the file in filename 
        featstream files have samples every 10 ms
    """
    with open(filename) as f:
        lineVectors = map(lambda s: [float(fs) for fs in s.strip().split(" ")], f.readlines())	# Format of audio.mfc is (Zahl Zahl Zahl)
    #lineVectors = map(lambda s: [float(fs.replace(',', '')) for fs in s.strip().split(" ")], f.readlines())	# Format of audio.mfc is (Zahl, Zahl, Zahl)
    subsampledVectors = []
    for column in zip(*lineVectors):
        subsampledVectors.append([smartMean(x) for x in grouper(column, subsample)])
        subsampledVectors.append([smartStdDev(x) for x in grouper(column, subsample)])
    return list(zip(*subsampledVectors))


class Poem:

    annotations : list
    labLines : list
    textLines : list

    def __init__(self, filenameBase, className, poemFormat='lab'):
        self.filenameBase = filenameBase  # the base of the filename; add .wav for the audio, .mfc for mfccs, .lab for label file, .txt for text file, ...
        self.className = className  # the name of the class this poem belongs to. e.g. "lettristicDecomposition"
        if poemFormat == 'lab':
            self.loadLabLines()  # a list of all lab lines as they occur in the .lab file
            if not args.ignore_audio:
                self.loadAudioFeatures()
            if not args.ignore_anno:
                self.loadAnnoLines()
        elif poemFormat == 'txt':
            self.loadTxtLines()
        elif poemFormat == 'txt-headerless':
            self.loadTxtLines(headerless=True)
        else:
            print('ERRR: invalid format {} for filename {}'.format(poemFormat, filenameBase))
            sys.exit(1)

    def __repr__(self):
        return self.filenameBase

    def loadLabLines(self):
        with open(self.fileFor(args.poem_lab_suffix)) as lab:
            self.labLines = lab.readlines()
            self.textLines = [line.split(" ", maxsplit=2)[2].rstrip() for line in self.labLines]

    def loadTxtLines(self, headerless=False):
        with open(self.fileFor(args.poem_txt_suffix)) as txt:
            self.title = ''
            if not headerless:
                headerline = txt.readline()
                while headerline != "\n":
                    self.title += headerline
                    headerline = txt.readline()
            self.textLines = [l.rstrip() for l in txt.readlines()]

    def loadAnnoLines(self):
        if ('line_annotation_suffix' in args and args.line_annotation_suffix != None):
            with open(self.fileFor(args.line_annotation_suffix)) as anno:
                #TODO: it would be safer to check that annotations are aligned
                self.annotations=[ast.literal_eval(line.split("\t")[-1]) for line in anno.readlines()]

    def discardAndAnnotatePunctuation(self):
        if not hasattr(self, "annotations"):
            # create annotations array if we don't have it already
            self.annotations = [{} for _ in range(len(self.textLines))]
        for i in range(len(self.textLines)):
            line = self.textLines[i]
            matches = list(re.finditer(r'.([\.\,\;\:\!\?/]).', line)) # initial and final "." to not find initial/final punctuation
            if len(matches) == 1: # this is a line that has exactly one inner punctuation
                match = matches[0]
                line = line[0:match.start(1)] + line[match.end(1):len(line)] # remove inner punctuation
                self.textLines[i] = line
                self.annotations[i]["middlepunctuation"] = "j"
                self.annotations[i]["punctuationafter"] = match.start(1) + 1
            elif len(matches) > 1:
                self.annotations[i]["middlepunctuation"] = "multiple"
            else:
                self.annotations[i]["middlepunctuation"] = "n"
#        print("in poem {} there are {} lines with and {} lines without punctuation".format(
#            self,
#            len([0 for x in self.annotations if x["middlepunctuation"] == "j"]),
#            len([0 for x in self.annotations if x["middlepunctuation"] == "n"])
#        ))


    def loadAudioFeatures(self):
        fullList = [m + f for m, f in zip(loadFeatStreamFile(self.fileFor('/audio.mfc')),
                                          loadFeatStreamFile(self.fileFor('/audio.ffv')))]
        lineAudio = []
        lineFollowingAudio = []
        prevEnd = None
        for line in self.labLines:
            (start, end, _) = line.split(" ", maxsplit=2)
            start = int(float(start) * 100 / args.audio_seq_step + .5)  # seconds to frames, account for aggregation
            end = int(float(end) * 100 / args.audio_seq_step + .5)
            # print("for line {} with start/end {}/{} appending {}".format(line, start, end, fullList[start:end]))
            if (start >= end):
                print('WARNING: line is too short; artificially extending it to make things work\t{}'.format(line))
                end = start + 1
            lineAudio.append(fullList[start:end])
            if prevEnd != None:
                lineFollowingAudio.append(fullList[prevEnd:start])
            prevEnd = end
        lineFollowingAudio.append(fullList[prevEnd:prevEnd+1])
        self.lineAudio = lineAudio
        self.lineFollowingAudio = lineFollowingAudio

    def fileFor(self, suffix):
        return os.path.join(args.data_path, self.filenameBase) + suffix

    def getClass(self):
        return self.className

    def getShortDescription(self):
        return self.getAllText()[0:20]

    def getLines(self):
        """return a list containing the text of every line"""
        return self.textLines

    def getLinesAndAudio(self):
        """return a list of tuples (linetext, list_of_audio_features)"""
        return zip(self.textLines,
                   self.lineAudio if hasattr(self, "lineAudio") else [None for x in self.textLines],
                   self.lineFollowingAudio if hasattr(self, "lineFollowingAudio") else [None for x in self.textLines],
                   self.annotations if hasattr(self, 'annotations') else [dict() for x in self.textLines])

    def getAudio(self):
        return self.lineAudio

    def normalizeAudio(self, means, stddev):
        lineAudio = self.lineAudio
        self.lineAudio = [((x - means) / stddev).tolist() for x in lineAudio]

    def getAllText(self):
        return "\n".join(self.getLines())

    def getAllCharacterTypes(self):
        return set("".join(self.getLines()))


class SaveableModel(object):
    def save(self, path):
        if not os.path.exists(path): os.makedirs(path)
        self.model.save(path + "/params")
        with open(path+"/args", "wb") as f: pickle.dump(self.args, f)


class LineClassifier(SaveableModel):
    def __init__(self, model, params, args):
        self.model = model
        self.params = dict(params)
        self.args = args
        self.add_params()

    def add_params(self):
        # per-line text representation
        self.params["fwdCharLSTM"] = dy.GRUBuilder(args.line_seq_layers, args.char_embed_size, args.line_seq_hidden_size, self.model)
        self.params["bwdCharLSTM"] = dy.GRUBuilder(args.line_seq_layers, args.char_embed_size, args.line_seq_hidden_size, self.model)
        # per-line audio representation
        self.params["fwdAudioLSTM"] = dy.GRUBuilder(args.audio_seq_layers, self.args.AUDIO_DIM, args.audio_seq_hidden_size, self.model)
        self.params["bwdAudioLSTM"] = dy.GRUBuilder(args.audio_seq_layers, self.args.AUDIO_DIM, args.audio_seq_hidden_size, self.model)
        # between-line audio representation
        self.params["fwdPauseLSTM"] = dy.GRUBuilder(args.pause_seq_layers, self.args.AUDIO_DIM, args.pause_seq_hidden_size, self.model)
        self.params["bwdPauseLSTM"] = dy.GRUBuilder(args.pause_seq_layers, self.args.AUDIO_DIM, args.pause_seq_hidden_size, self.model)
        if (args.no_pretraining):
            self.params["char_lookup"] = self.model.add_lookup_parameters((self.params["CHAR_INPUT_DIM"], args.char_embed_size))
            self.params["charAtt_MLP_W"] = self.model.add_parameters((args.line_attention_size, args.line_seq_hidden_size * 2))
            self.params["charAtt_MLP_bias"] = self.model.add_parameters((args.line_attention_size))
            self.params["charAtt_U"] = self.model.add_parameters((1, args.line_attention_size))
            self.params["audioAtt_MLP_W"] = self.model.add_parameters((args.audio_attention_size, args.audio_seq_hidden_size * 2))
            self.params["audioAtt_MLP_bias"] = self.model.add_parameters((args.audio_attention_size))
            self.params["audioAtt_U"] = self.model.add_parameters((1, args.audio_attention_size))
            self.params["pauseAtt_MLP_W"] = self.model.add_parameters((args.pause_attention_size, args.pause_seq_hidden_size * 2))
            self.params["pauseAtt_MLP_bias"] = self.model.add_parameters((args.pause_attention_size))
            self.params["pauseAtt_U"] = self.model.add_parameters((1, args.pause_attention_size))
        else:
            self.params["char_lookup"] = self.model.load_lookup_param('pretraining/pretraining.dypar', '/charLookup')
            self.params["fwdCharLSTM"].param_collection().populate('pretraining/pretraining.dypar', '/gru-builder/')
            self.params["bwdCharLSTM"].param_collection().populate('pretraining/pretraining.dypar', '/gru-builder_1/')
            self.params["charAtt_MLP_W"] = self.model.load_param('pretraining/pretraining.dypar', '/charAttMLPW')
            self.params["charAtt_MLP_bias"] = self.model.load_param('pretraining/pretraining.dypar', '/charAttMLPbias')
            self.params["charAtt_U"] = self.model.load_param('pretraining/pretraining.dypar', '/charAttU')
            self.params["fwdPauseLSTM"].param_collection().populate('pretraining/silencePretraining.dypar', '/gru-builder/')
            self.params["bwdPauseLSTM"].param_collection().populate('pretraining/silencePretraining.dypar',
                                                               '/gru-builder_1/')
            self.params["fwdAudioLSTM"].param_collection().populate('pretraining/audioPretraining.dypar', '/gru-builder/')
            self.params["bwdAudioLSTM"].param_collection().populate('pretraining/audioPretraining.dypar', '/gru-builder_1/')
            self.params["audioAtt_MLP_W"] = self.model.load_param('pretraining/audioPretraining.dypar', '/audioAttMLPW')
            self.params["audioAtt_MLP_bias"] = self.model.load_param('pretraining/audioPretraining.dypar', '/audioAttMLPbias')
            self.params["audioAtt_U"] = self.model.load_param('pretraining/audioPretraining.dypar', '/audioAttU')
            self.params["pauseAtt_MLP_W"] = self.model.load_param('pretraining/silencePretraining.dypar', '/audioAttMLPW')
            self.params["pauseAtt_MLP_bias"] = self.model.load_param('pretraining/silencePretraining.dypar', '/audioAttMLPbias')
            self.params["pauseAtt_U"] = self.model.load_param('pretraining/silencePretraining.dypar', '/audioAttU')

        # per-line decision-making (for initial pre-training iteration)
        lineDecLayerInputDims = ((args.line_seq_hidden_size * 2 if not args.ignore_text else 0)
                                 + ((args.audio_seq_hidden_size
                                     + (args.pause_seq_hidden_size if not args.ignore_pause else 0))
                                    * 2 if not args.ignore_audio else 0)
                                 + (args.line_feature_count if args.line_features and not args.ignore_anno else 0))

        self.params["lineDecisionMLP_W"] = self.model.add_parameters((self.params["NUM_CLASSES"], lineDecLayerInputDims))  # Softmax weights
        self.params["lineDecisionMLP_bias"] = self.model.add_parameters((self.params["NUM_CLASSES"]))  # Softmax bias


class HANClassifier(LineClassifier):
    def __init__(self, model, params, args):
        LineClassifier.__init__(self, model, params, args)


    @staticmethod
    def load(model, path, load_model_params=True):
        if not os.path.exists(path): raise Exception("Model "+path+" does not exist")
        with open(path+"/args", "rb") as f: args = pickle.load(f)
        args.no_pretraining = True # we'll load the parameters anyway so no need to pre-fetch them.
        classifier = HANClassifier(model, args)
        classifier.model.populate(path+"/params")
        return classifier


    def add_params(self):
        super().add_params()
        # per-line decision-making (for initial pre-training iteration)
        lineDecLayerInputDims = ((args.line_seq_hidden_size * 2 if not args.ignore_text else 0)
                                 + ((args.audio_seq_hidden_size
                                     + (args.pause_seq_hidden_size if not args.ignore_pause else 0))
                                    * 2 if not args.ignore_audio else 0)
                                 + (args.line_feature_count if args.line_features and not args.ignore_anno else 0))
        # per-poem representation of line-outputs
        self.params["fwdLineLSTM"] = dy.GRUBuilder(args.poem_seq_layers, lineDecLayerInputDims, args.poem_seq_hidden_size,
                                                   self.model)
        self.params["bwdLineLSTM"] = dy.GRUBuilder(args.poem_seq_layers, lineDecLayerInputDims, args.poem_seq_hidden_size,
                                                   self.model)
        if args.ignore_audio and not args.no_pretraining:  # load pre-trained poem-models that were built on text alone
            self.params["fwdLineLSTM"].param_collection().populate('pretraining/poempretraining.dypar', '/gru-builder_2')
            self.params["bwdLineLSTM"].param_collection().populate('pretraining/poempretraining.dypar', '/gru-builder_3')
            self.params["lineAtt_MLP_W"] = self.model.load_param('pretraining/poempretraining.dypar', '/lineAttMLPW')
            self.params["lineAtt_MLP_bias"] = self.model.load_param('pretraining/poempretraining.dypar', '/lineAttMLPbias')
            self.params["lineAtt_U"] = self.model.load_param('pretraining/poempretraining.dypar', '/lineAttMLPU')
        else:  # we do not have properly pre-trained poem-models that include audio
            self.params["lineAtt_MLP_W"] = self.model.add_parameters((args.poem_attention_size, args.poem_seq_hidden_size * 2))
            self.params["lineAtt_MLP_bias"] = self.model.add_parameters((args.poem_attention_size))
            self.params["lineAtt_U"] = self.model.add_parameters((1, args.poem_attention_size))

        # per-poem decision-making
        poemDecLayerInputDims = args.poem_seq_hidden_size * 2
        self.params["poemDecisionMLP_W"] = self.model.add_parameters((self.params["NUM_CLASSES"], poemDecLayerInputDims))  # Softmax weights
        self.params["poemDecisionMLP_bias"] = self.model.add_parameters((self.params["NUM_CLASSES"]))  # Softmax bias

        if args.VISUALIZATION_MODE:
            # to plot poems in N-dimensional space, we create something that passes through a narrow layer before the softmax
            self.params["poemVisualizationLow_W"] = self.model.add_parameters((args.visualization_dimensions, poemDecLayerInputDims))
            self.params["poemVisualizationLow_b"] = self.model.add_parameters((args.visualization_dimensions))
            self.params["poemVisualizationMLP_W"] = self.model.add_parameters((self.params["NUM_CLASSES"], args.visualization_dimensions))
            self.params["poemVisualizationMLP_b"] = self.model.add_parameters((self.params["NUM_CLASSES"]))


def labelFromLabelLine(line):
    #    return " ".join(line.split()[2:]) # too simple: merges together multiple blanks within a line
    return line.split(" ", maxsplit=2)[2]


def normalize_acoustics(poems):
    featVectors = []
    for poem in poems:
        for lineAudio in poem.getAudio():
            featVectors.extend(lineAudio)
    means = np.mean(featVectors, axis=0)
    stddevs = np.std(featVectors, axis=0)
    for poem in poems:
        poem.normalizeAudio(means, stddevs)


def loadFromFiles(masterFiles, poemFormat='lab'):
    all_data = defaultdict(lambda: [])
    all_char_types = set()
    for masterFile in masterFiles:
        (data, char_types) = loadFromFile(masterFile, poemFormat)
        for (key, vals) in data.items():
            all_data[key].extend(vals)
            all_char_types = all_char_types.union(char_types)
    return (dict(all_data), list(all_char_types))


def loadFromFile(masterFile, poemFormat='lab'):
    data = defaultdict(lambda: [])
    char_types = set()
    if masterFile.endswith('.pickle'):
        with open(masterFile, 'rb') as f:
            data = pickle.load(f)
            char_types = pickle.load(f)
            pickled_audio_seq_step = pickle.load(f)
            if pickled_audio_seq_step != args.audio_seq_step:
                print('WARN: audio_seq_step in pickled data is {} (not what was specified as argument)'.format(
                    pickled_audio_seq_step))
    else:
        all_poems = []
        with open(masterFile) as f:
            for line in f.readlines():
                tokens = line.strip().split("\t")
                assert len(tokens) <= 2, "malformatted line: {}".format(line)
                key = tokens[0] if len(tokens) == 2 else masterFile # use name of file as default
                poemFile = tokens[-1] # the last token
                print("INFO: reading {}".format(poemFile))
                poem = Poem(poemFile, key, poemFormat)
                if (poem.getShortDescription() == ''):
                    print("WARN: discarding file with empty poem text: {}".format(poemFile))
                else:
                    char_types = char_types.union(poem.getAllCharacterTypes())
                    data[key].append(poem)
                    all_poems.append(poem)
        data = dict(data)  # we're done using the defaultdict functionality
        char_types.add('<EOL>')
        if args.normalize_acoustics and not args.ignore_audio:
            normalize_acoustics(all_poems)
    if args.store_pickle:
        if masterFile.endswith('.pickle'):
            print('not re-pickling pickle file.')
        else:
            with open(masterFile + '.pickle', 'wb') as f:
                pickle.dump(data, f)
                pickle.dump(char_types, f)
                pickle.dump(args.audio_seq_step, f)
                f.close()
    return (data, list(char_types))


def lookup_text_for_rnn(inputText, params):
    inputCodes = [params["char2int"]['<EOL>']]
    inputCodes.extend([params["char2int"][c] for c in list(inputText)])
    inputCodes.append(params["char2int"]['<EOL>'])
    inputEmbds = [params["char_lookup"][c] for c in inputCodes]
    return inputEmbds


def run_char_rnn(params, lstmName, inputText, isTrain=False):
    inputEmbds = lookup_text_for_rnn(inputText, params)
    return run_rnn(params, lstmName, inputEmbds, isTrain)


def run_rnn(params, lstmName, inputTensor, isTrain=False):
    #    print(inputTensor)
    lstm = params[lstmName]
    if isTrain:
        lstm.set_dropout(args.dropout)
    return lstm.initial_state().transduce(inputTensor)


def attend(output_vectors, params, attentionName, attentionStore=None):
    MLP_W = params[attentionName + "_MLP_W"]
    dy.dropout(MLP_W, args.dropout)
    MLP_bias = params[attentionName + "_MLP_bias"]
    U = params[attentionName + "_U"]
    output_matrix = dy.concatenate_cols(output_vectors)
    unnormalized = dy.transpose(U * dy.tanh(dy.colwise_add(MLP_W * output_matrix, MLP_bias)))
    att_weights = dy.softmax(unnormalized)
    if attentionStore != None:
        attentionStore.extend(att_weights)
    context = output_matrix * att_weights
    return context


def aggregateSeqResults(fwd_output, bwd_output, params, attType, attentionStore=None):
    # method for no attention: use the final output from each sequence:
    # output = dy.concatenate([fwd_output[-1], bwd_output[-1]]) # first because we've already reversed above
    # output = (fwd_output[-1] + bwd_output[-1]) / 2
    # with attention:
    full_output = [dy.concatenate(list(p)) for p in zip(fwd_output, list(reversed(bwd_output)))]
    output = attend(full_output, params, attType + "Att", attentionStore)
    return output


def compute_audio_representation(inputAudio, lstmTypeName, params, isTrain):
    inputAudio = [[0 for x in range(args.AUDIO_DIM)]] + inputAudio + [[0 for x in range(args.AUDIO_DIM)]]
    audioTensor = dy.inputTensor(inputAudio)
    fwd_audio_output = run_rnn(params, "fwd" + lstmTypeName + "LSTM", audioTensor, isTrain)
    bwdAudioTensor = dy.inputTensor(list(reversed(inputAudio)))
    bwd_audio_output = run_rnn(params, "bwd" + lstmTypeName + "LSTM", bwdAudioTensor, isTrain)
    audio_output = aggregateSeqResults(fwd_audio_output, bwd_audio_output, params, lstmTypeName.lower())
    return audio_output


def get_line_loss(className, inputText, inputAudio, pauseAudio, lineAnnotation, params):
    mlp_output = compute_line_network_decision(inputText, inputAudio, pauseAudio, lineAnnotation, params, isTrain=True)
    loss = dy.pickneglogsoftmax(mlp_output, params["class2int"][className])
    return loss


def compute_line_network_decision(inputText, inputAudio, pauseAudio, lineAnnotation, params, isTrain=False, attentionStore=None):
    dy.renew_cg()
    line_output = compute_line_network_output(inputText, inputAudio, pauseAudio, lineAnnotation, params, isTrain, attentionStore)
    MLP_W = params["lineDecisionMLP_W"]
    MLP_bias = params["lineDecisionMLP_bias"]
    if isTrain:
        dy.dropout(MLP_W, args.dropout)
    return dy.tanh(MLP_W * line_output + MLP_bias)


def compute_line_network_output(inputText, inputAudio, pauseAudio, lineAnnotation, params, isTrain=False, attentionStore=None):
    sub_vectors = [] # collect output from sub-LSTMs
    if args.ignore_text:
        inputText = None
    # process character text:
    if (inputText != None):
        fwd_char_output = run_char_rnn(params, "fwdCharLSTM", inputText, isTrain)
        bwd_char_output = run_char_rnn(params, "bwdCharLSTM", reversed(inputText), isTrain)
        char_output = aggregateSeqResults(fwd_char_output, bwd_char_output, params, "char", attentionStore)
        sub_vectors.append(char_output)
    # process audio for the line
    if not args.ignore_audio and inputAudio != None:
        audio_output = compute_audio_representation(inputAudio, "Audio", params, isTrain)
        sub_vectors.append(audio_output)
        if not args.ignore_pause and pauseAudio != None:
            pause_output = compute_audio_representation(pauseAudio, "Pause", params, isTrain)
            sub_vectors.append(pause_output)
    if args.line_features and not args.ignore_anno:
        one_hot_feature_vector = []
        for feat,values in args.line_feature_dictionary.items():
            for value in values:
                one_hot_feature_vector.append(1.0 if lineAnnotation and feat in lineAnnotation and lineAnnotation[feat] == value else 0.0)
        sub_vectors.append(dy.inputVector(one_hot_feature_vector))
    line_output = dy.concatenate(sub_vectors)
    return line_output


def unfoldClassData(classData):
    for (clas, poemlist) in classData.items():
        for poem in poemlist:
            yield (clas, poem)


def print_f_scores(matrix, classNames=None):
    (x, y) = np.shape(matrix)
    assert x == y, "need square matrix to compute f-measure"
    NUM_CLASSES = x
    TP = np.diag(matrix)
    FP = np.sum(matrix, axis=0) - TP
    FN = np.sum(matrix, axis=1) - TP
    TN = []
    for i in range(NUM_CLASSES):
        temp = np.delete(matrix, i, 0)
        temp = np.delete(temp, i, 1)
        TN.append(np.sum(temp))
    F = 2*TP / (2*TP + FN + FP)
    print("per-class f-measures:")
    for i in range(NUM_CLASSES):
        print('{}\t{}\t{}'.format(i, F[i], classNames[i] if classNames else ""))#params["int2class"][NUM_CLASSES], F[i]))
    classCounts = np.sum(matrix, axis=1)
    weights = classCounts / np.sum(classCounts)
    weightedF = np.sum(F * weights)
    print("weighted f-measure: {}".format(weightedF))


def colorsForWeights(att_weights):
    '''convert a list of dynet expressions to a list of colors indicating the weights'''
    att_weights = [x.value() for x in att_weights]
    maxVal = max(att_weights)
    weightVals = [(int(255.0*x/maxVal),0,0) for x in att_weights]
    return weightVals


