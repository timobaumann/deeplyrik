#!/usr/bin/env python3
import argparse

# TODO: there should be a mode without attention
# TODO: there should be a mode in which classification is based on majority of the lines
# TODO: integrate analysis code for relative attention per line-annotations from textAudioClassifierWithEnjambment.py

parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('--random_seed', type=int, default=1, help='set dynet and python random seeds for replicability; 0 implies truly random seed')
parser.add_argument('--no_sleep', action='store_true', help='switch off random startup sleep (useful on NFS-based clusters for multi-processing)')
parser.add_argument('--char_embed_size', type=int, default=20)
parser.add_argument('--line_seq_layers', type=int, default=2)
parser.add_argument('--line_seq_hidden_size', type=int, default=20)
# TODO: Option um attention vs. final output (potentially more possibilities (mean, max, ...))
parser.add_argument('--line_attention_size', type=int, default=20)
parser.add_argument('--ignore_audio', action='store_true')
parser.add_argument('--ignore_pause', action='store_true')
parser.add_argument('--ignore_text', action='store_true')
parser.add_argument('--ignore_anno', action='store_true')
parser.add_argument('--no_pretraining', action='store_true')
#parser.add_argument('--rnn_aggregation', choice={'attention', 'last'}, default='attention')
parser.add_argument('--normalize_acoustics', type=bool, default=True)
parser.add_argument('--audio_seq_step', type=int, default=10, help='how many feature frames to combine; will be overridden by pickle\'d value') # 100ms
parser.add_argument('--audio_seq_layers', type=int, default=2)
parser.add_argument('--audio_seq_hidden_size', type=int, default=30)
parser.add_argument('--audio_attention_size', type=int, default=25)
parser.add_argument('--pause_seq_layers', type=int, default=1)
parser.add_argument('--pause_seq_hidden_size', type=int, default=20)
parser.add_argument('--pause_attention_size', type=int, default=15)
parser.add_argument('--poem_seq_layers', type=int, default=2)
parser.add_argument('--poem_seq_hidden_size', type=int, default=20)
parser.add_argument('--poem_attention_size', type=int, default=20)
parser.add_argument('--line_annotation_suffix', type=str, default='/txt.lab.anno', help='file suffix for line-by-line annotations')
parser.add_argument('--line_features', type=str, default='', help='use line feature annotations, e.g.: "enjambment=j,n,u;ellipse=j,n" and their classes')
parser.add_argument('--dropout', type=float, default=0.2, help='dropout probability (applied on all layers)')
parser.add_argument('--iterations', type=int, default=30, help='training iterations for the network; use fewer iterations without dropout')
parser.add_argument('--test_folds', type=int, help='number of folds during cross-fold validation')
parser.add_argument('--test_set', type=str, help='use a specific test-set (rather than cross-fold validation; make sure the test set has class assignments)')
parser.add_argument('--apply_to', type=str, help='apply classifier to a given set of poems')
parser.add_argument('--data_path', type=str, default='', help='path to data files; UNUSED if using pickled data') # /home/timo/Dropbox/Prosodische_Muster_Gedicht/
parser.add_argument('--trials', type=int, default=1, help='the number of times to run classification for each file to balance out effects from random initialization')
parser.add_argument('--store_pickle', default=True, action='store_true', help='store a pickle of the loaded plain text input poem file')
parser.add_argument('--dynet_autobatch', type=int, default=1)
# TODO: implement storing model(s). if we perform multiple runs (e.g. for X-fold validation or multiple trials), each will be stored in its own numbered subdirectory
parser.add_argument('--save_model', type=str, help='location to save the generated model(s) to')
# TODO: implement loading of model
parser.add_argument('--load_model', type=str, help='location to load a given model from')
parser.add_argument('input_poems_file', nargs='+', help='specification of poems and assigned classes; either plain text (see README) or pickle (if suffix is .pickle)')
args = parser.parse_args()
args.poem_lab_suffix = '/txt.lab'
import re
if args.line_features:
    if not re.match("[a-zA-Z]+=[a-zA-Z]+(,[a-zA-Z]+)*,[a-zA-Z]+(;[a-zA-Z]+=[a-zA-Z]+(,[a-zA-Z]+)*,[a-zA-Z]+)*", args.line_features):
        raise RuntimeError("illegal format of --line_features argument")
    args.line_feature_dictionary = dict(map(lambda x: (x.split('=')[0], x.split('=')[1].split(',')), args.line_features.split(';')))
    args.line_feature_count = sum([len(x) for x in args.line_feature_dictionary.values()])
if args.test_set or args.apply_to or args.load_model:
    assert args.test_folds == None, "X-fold validation is mutually exclusive with loading model, application and external test set"
    args.test_folds = 1
print(args)

args.VISUALIZATION_MODE = False
args.visualization_dimensions = 1

import dynet_config
dynet_config.set(random_seed=args.random_seed, autobatch=args.dynet_autobatch, mem=1536)
import dynet as dy
import datetime
from random import shuffle, random, seed
if args.random_seed != 0:
    seed(args.random_seed)
from scipy.stats import binom_test
from time import sleep
from colors import color # color on the commandline!, corresponding module is called "ansicolors" in pip
import deeplyriklib as dll
import numpy as np
from collections import defaultdict

AUDIO_DIM = 2 * (13 + 7) # 13 mfcc, 7 ffv, means and stddev of each

args.AUDIO_DIM = AUDIO_DIM
dll.args = args

if not args.no_sleep:
    sleep_time = random() * 8
    print("sleeping for {:.3} s".format(sleep_time))
    sleep(sleep_time)


def compute_poem_network_decision(poem, params, isTrain=False, attentionStore=None):
    dy.renew_cg()
    line_outputs = []
    lines = [l for l in poem.getLinesAndAudio() if l[0] != ""]        
    for line in lines:
        line_output = dll.compute_line_network_output(line[0], line[1], line[2], line[3], params, isTrain)
        line_outputs.append(line_output)
    fwd_poem_output = dll.run_rnn(params, "fwdLineLSTM", line_outputs, isTrain)
    bwd_poem_output = dll.run_rnn(params, "bwdLineLSTM", list(reversed(line_outputs)), isTrain)
    poem_output = dll.aggregateSeqResults(fwd_poem_output, bwd_poem_output, params, "line", attentionStore)
    if not args.VISUALIZATION_MODE:
        MLP_W = params["poemDecisionMLP_W"]
        MLP_b = params["poemDecisionMLP_bias"]
        if isTrain:
            dy.dropout(MLP_W, args.dropout)
        return (dy.tanh(MLP_W * poem_output + MLP_b), None)
    else:
        Low_W = params["poemVisualizationLow_W"]
        Low_b = params["poemVisualizationLow_b"]
        MLP_W = params["poemVisualizationMLP_W"]
        MLP_b = params["poemVisualizationMLP_b"]
        if isTrain:
            dy.dropout(MLP_W, args.dropout)
        poem_bottleneck = (Low_W * poem_output + Low_b)
        return dy.tanh(MLP_W * poem_bottleneck + MLP_b), poem_bottleneck


def get_poem_loss(className, poem, params):
    (mlp_output, _) = compute_poem_network_decision(poem, params, isTrain=True)
    loss = dy.pickneglogsoftmax(mlp_output, params["class2int"][className])
    return loss


def trainOnLines(trainLines, trainer, classifier, iterations):
    for iteration in range(iterations):
        shuffle(trainLines)
        i = 0
        aggrloss = 0
        for (className, line) in trainLines:
            loss = dll.get_line_loss(className, line[0], line[1], line[2], line[3], classifier.params)
            loss_value = loss.value()
            aggrloss += loss_value
            loss.forward()
            loss.backward()
            trainer.update()
            i += 1
        print("per-line iteration:\t{:02},\tavg. loss:\t{:.3}\r".format(iteration, aggrloss/i), end='')
    print() # add newline (above print does not print newlines)


def testOnLines(testLines, classifier, testResults):
    testCorrect = 0
    for (className, line) in testLines:
        # a "line" contains both text and audio, but not the class label
        att_weights = []
        mlp_output = dll.compute_line_network_decision(line[0], line[1], line[2], line[3], classifier.params, attentionStore=att_weights)
        output = mlp_output.value()
        best = max([(v,i) for (i,v) in enumerate(output)])
        estimatedClass = best[1]
        estimatedName = classifier.params["int2class"][estimatedClass]
        realClass = classifier.params["class2int"][className]
        correct = estimatedClass == realClass
        #testResultClasses[params["int2class"][best[1]]] += 1
        testResults["total"] += 1
        testResults["correct"] += 1 if correct else 0
        testCorrect += 1 if correct else 0
        testResults["confusionMap"][realClass, estimatedClass] += 1 #["{}->{}".format(className, params["int2class"][best[1]])] += 1
        loss = dy.pickneglogsoftmax(mlp_output, best[1])
        print("classified as:\t{},\tloss:\t{:5.3f}\t".format(estimatedName, loss.value()), end='')
        if (not args.ignore_text):
            colors = dll.colorsForWeights(att_weights)
            for (char, charcolor) in zip(' ' + line[0] + ' ', colors):
                print(color(char, fg='white', bg=charcolor), end='')
            print()
        else:
            print(line[0])
    return testCorrect


def trainOnPoems(trainPoems, trainer, classifier, iterations):
    for iteration in range(iterations):
        shuffle(trainPoems)
        i = 0
        aggrloss = 0
        for poem in trainPoems:
            className = poem.getClass()
            loss = get_poem_loss(className, poem, classifier.params)
            loss_value = loss.value()
            aggrloss += loss_value
            loss.forward()
            loss.backward()
            trainer.update()
            i += 1
        print("per-poem iteration:\t{:02},\tavg. loss:\t{:.3}\r".format(iteration, aggrloss/i), end='')
    print() # add newline (above print does not print newlines)


def testOnPoems(testPoems, classifier, testResults):
    for poem in testPoems:
        att_weights = []
        (mlp_output, bottleneck) = compute_poem_network_decision(poem, classifier.params, attentionStore=att_weights)
        output = mlp_output.value()
        realClass = classifier.params["class2int"][poem.getClass()]
        if args.VISUALIZATION_MODE:
            print("{} {} {} {}".format(" ".join([str(f) for f in bottleneck.vec_value()]),
                                       realClass,
                                       poem.getClass(),
                                       poem.filenameBase),
                  )
        else:
            colors = dll.colorsForWeights(att_weights)
            lines = [l for l in poem.getLinesAndAudio() if l[0] != '']
            for (line, annotation, linecolor, att_weight) in zip([l[0] for l in lines],
                                                     [l[3] for l in lines],
                                                     colors, att_weights):
                print(color(str(annotation) + line, fg='white', bg=linecolor))
            best = max([(v,i) for (i,v) in enumerate(output)])
            estimatedClass = best[1]
            estimatedName = classifier.params["int2class"][estimatedClass]
            if args.apply_to:
                poem.estimatedClassName = estimatedName
            correct = estimatedClass == realClass
            testResults["total"] += 1
            if correct: testResults["correct"] += 1
            testResults["confusionMap"][realClass, estimatedClass] += 1
            loss = dy.pickneglogsoftmax(mlp_output, best[1])
            ## TODO: try out better methods for confidence computation
            confidence = 1 - loss.value()
            if correct:
                testResults["correctConf"].append(confidence)
            else:
                testResults["incorrectConf"].append(confidence)
            print("poem classified as:\t{},\tloss:\t{:5.3f}\t{}".format(estimatedName, loss.value(), "correct" if correct else "WRONG"))


def trainAndTest(trainPoems, classifier, trainer, iterations=50, testPoems=[], testResults=[]):
    poemsCorrect = 0
    ### unfold training poems into individual lines
    trainLines = unfoldToLineData(trainPoems)
    testLines = unfoldToLineData(testPoems)

    trainOnLines(trainLines, trainer, classifier, iterations)
    # perform per-line evaluation and print (brief) stats
    if testPoems and not args.apply_to:
        linesCorrect = testOnLines(testLines, classifier, testResults=testResults["linesIndiv"])
    #print("lines: {}/{} ({:5.1f}%)\tsimple heuristics: {}".format(linesCorrect, len(testLines), 100.0*linesCorrect/len(testLines), linesCorrect/len(testLines) >= 0.5))
    #trainOnPoems(trainPoems, trainer, classifier, iterations)
    # perform per-poem evaluation
    #if testPoems:
    #    testOnPoems(testPoems, classifier, testResults=testResults["poems"])


def unfoldToLineData(poems):
    # ignore lines without text ("")
    return [(p.getClass(), l) for p in poems for l in p.getLinesAndAudio() if l[0] != ""]


def prepareXCrossFolds(classData):
    folds = [[] for x in range(args.test_folds)]
    # for every class, shuffle items in class, add items to buckets, shuffle buckets, add buckets to folds
    for (poemClass, poems) in classData.items():
        shuffle(poems)
        classFolds = [[] for x in range(args.test_folds)]
        for i in range(len(poems)):
            classFolds[i % args.test_folds].append(poems[i])
        shuffle(classFolds)
        for i in range(len(folds)):
            folds[i].extend(classFolds[i])
    return expandFoldsToTrainTest(folds)

def prepareBySpeakerFolds(classData):
    byID = defaultdict(lambda : [])
    identifyingRE = r'^.........' # first 9 characters of the identifying name
    for (poemClass, poems) in classData.items():
        for poem in poems:
            match = re.search(identifyingRE, poem.filenameBase)
            assert match, "could not match on {}".format(poem.filenameBase)
            byID[match.group(0)].append(poem)
    #print(byID)
    return expandFoldsToTrainTest(byID.values())


def expandFoldsToTrainTest(folds):
    trainTestPairs = []
    # prepare folds of training/test data:
    for fold in folds:
        testPoems = fold
        trainPoems = []
        for fold in folds:
            if fold != testPoems:
                trainPoems.extend(fold)
        trainTestPairs.append((trainPoems, testPoems))
    return trainTestPairs


def main():
    """start with one tab-separated-values file as parameter. 
       Each line should contain info for one poem: 
       the poem's class, path to file containing the poem's text"""
    print(color("started: {}".format(datetime.datetime.now()), fg='green'))
    print("loading training data from [{}]".format(', '.join(args.input_poems_file)))
    (classData, observed_char_types) = dll.loadFromFiles(args.input_poems_file, poemFormat='lab')
    allClasses = set(classData.keys())
    if args.test_set or args.apply_to:
        print("loading test data from {}".format(args.test_set or args.apply_to))
        (testData, observed_char_types2) = dll.loadFromFiles([args.test_set or args.apply_to])
        observed_char_types = set(observed_char_types).union(observed_char_types2)
        allClasses = allClasses.union(set(testData.keys()))
    NUM_CLASSES = len(allClasses)

    CHAR_TYPES = ['\t', ' ', '!', '"', '#', '$', '%', '&', "'", '(', ')', '*', '+', ',', '-', '.', '/', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ':', ';', '<', '=', '>', '?', '@', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '[', ']', '^', '_', '`', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '|', '~', '©', '«', '¬', '\xad', '¯', '°', '´', '·', 'º', '»', 'Ä', 'Ç', 'É', 'Ó', 'Ö', '×', 'Ø', 'Ü', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'ö', 'ø', 'ú', 'û', 'ü', 'ā', 'ą', 'ē', 'ı', 'ł', 'ŉ', 'ŋ', 'š', 'ţ', 'ū', 'ż', 'ſ', 'ɘ', '̈', 'Α', 'Γ', 'Θ', 'Ι', 'Κ', 'Ν', 'Ο', 'Σ', 'Τ', 'Υ', 'Ω', 'α', 'β', 'γ', 'δ', 'ε', 'η', 'ι', 'λ', 'μ', 'ν', 'ο', 'π', 'σ', 'τ', 'ω', 'ϑ', 'ϱ', 'Б', 'В', 'Н', 'С', 'Ы', 'Э', 'а', 'в', 'г', 'д', 'е', 'ж', 'и', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'х', 'ц', 'ш', 'щ', 'ы', 'ь', 'э', 'أ', 'ا', 'ة', 'ر', 'ش', 'ض', 'ف', 'ἐ', 'Ἔ', 'ἣ', 'ὅ', 'Ὦ', 'ὴ', 'ώ', 'ῆ', 'ῖ', '‒', '–', '—', '‘', '’', '‚', '“', '”', '„', '†', '•', '…', '‹', '›', '№', '→', '−', '∙', '√', '│', '◊', '・', 'ﬂ', '<EOL>', 'č', "✴", "}",  "ˆ", "{", "²", "¸", ""]
    for char in observed_char_types:
        assert char in CHAR_TYPES, 'character "{}" in input data cannot be mapped to known char types'.format(char)
    INPUT_DIM = len(CHAR_TYPES)

    params = {}
    params["int2char"] = list(CHAR_TYPES)
    params["char2int"] = {c:i for i,c in enumerate(CHAR_TYPES)}
    params["int2class"] = list(allClasses)
    params["class2int"] = {c:i for i,c in enumerate(allClasses)}
    params["NUM_CLASSES"] = NUM_CLASSES
    params["CHAR_INPUT_DIM"] = INPUT_DIM
    allPoems = [x[1] for x in dll.unfoldClassData(classData)]
    numPoems = len(allPoems)

    runIndex = 0
    testResults = { "linesIndiv" : { "total" : 0, "correct" : 0, "correctConf" : [], "incorrectConf" : [], "confusionMap" : np.zeros(shape=[NUM_CLASSES, NUM_CLASSES])},
                    "linesGlobal" : { "total" : 0, "correct" : 0, "correctConf" : [], "incorrectConf" : [], "confusionMap" : np.zeros(shape=[NUM_CLASSES, NUM_CLASSES])},
                    "poems" : { "total" : 0, "correct" : 0, "correctConf" : [], "incorrectConf" : [], "confusionMap" : np.zeros(shape=[NUM_CLASSES, NUM_CLASSES])}
                  }
    assert args.test_folds <= numPoems, "test set size is too large for the given number of poems: {} vs. {}".format(args.test_folds, numPoems)

    for i in range(args.trials):
        if args.test_folds > 1:
            trainTestPairs = prepareXCrossFolds(classData)
#            trainTestPairs = prepareBySpeakerFolds(classData)
        else:
            trainTestPairs = [(allPoems, [x[1] for x in dll.unfoldClassData(testData)])]
        allRuns = len(trainTestPairs) * args.trials
        for (trainPoems, testPoems) in trainTestPairs:
            runIndex += 1
            print('training run {} of {} ({:5.1f}% done)'.format(runIndex, allRuns, 100*(runIndex-1) / allRuns))

            ### set up dynet anew
            model = dy.Model()
            classifier = dll.HANClassifier(model, params, args)
            trainer = dy.AdamTrainer(model)

            for testPoem in testPoems:
                print("for item in class '{}' with text '{}...'".format(testPoem.getClass(),
                                                                        testPoem.getShortDescription()))
            if args.VISUALIZATION_MODE:
                trainPoems = allPoems
                testPoems = allPoems
            trainAndTest(trainPoems, classifier, trainer, iterations=args.iterations, testPoems=testPoems, testResults=testResults)
#            print(testResults)
            majorityProportion = max([len(x) for x in classData.values()]) / numPoems
            testResults["poems"] = testResults["linesIndiv"]
            print(testResults)
            print("{} out of {} correct ({:5.1f}%), p<{:.3}, lines {:.3}".format(
              testResults["poems"]["correct"],
              testResults["poems"]["total"],
              testResults["poems"]["correct"] / testResults["poems"]["total"] * 100,
              binom_test(round(testResults["poems"]["correct"]),
                         testResults["poems"]["total"],
                         p=majorityProportion),
              testResults["linesIndiv"]["correct"] / testResults["linesIndiv"]["total"]))
            print("confusionMap: {}".format(testResults["poems"]["confusionMap"]))
            dll.print_f_scores(testResults["poems"]["confusionMap"], params["int2class"])
        if args.apply_to:
            # print classification results:
            for poem in testPoems:
                print("{} {}".format(poem.estimatedClassName, poem.filenameBase))
        else: # print evaluation results
            majorityProportion = max([len(x) for x in classData.values()]) / numPoems
            testResults["poems"] = testResults["linesIndiv"]
            print(testResults)
            print("{} out of {} correct ({:5.1f}%), p<{:.3}, lines {:.3}".format(
              testResults["poems"]["correct"],
              testResults["poems"]["total"],
              testResults["poems"]["correct"] / testResults["poems"]["total"] * 100,
              binom_test(round(testResults["poems"]["correct"]),
                         testResults["poems"]["total"],
                         p=majorityProportion),
              testResults["linesIndiv"]["correct"] / testResults["linesIndiv"]["total"]))
            print("confusionMap: {}".format(testResults["poems"]["confusionMap"]))
            dll.print_f_scores(testResults["poems"]["confusionMap"], params["int2class"])
        #for i in range(NUM_CLASSES):

    print(color("finished: {}".format(datetime.datetime.now()), fg='green'))


if __name__ == "__main__":
    main()
