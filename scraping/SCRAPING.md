# How to scrape Lyrikline

## Finding out about all poems on lyrikline (or all of a given type)

It's slightly tricky to get all poems from lyrikline because the search is stored 
in a session cookie and search pages merely contain "seite=n" markers. This complicates 
wget maneuvers because we need to store and re-use the session cookie, and need to send 
search arguments, and a URL. All this is dealt with in `scrapeSearch.py`. 
Call it with the search-URL for the first page of your search as a parameter. Make sure 
to quote it, otherwise it will certainly break (given the & and ? characters in there).

