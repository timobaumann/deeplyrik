#!/usr/bin/env python3
import html5lib
import re
from xml import etree
import codecs
import os

import argparse

parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('--test_only', action='store_true', help='only read files but do not write to SWC/TXT files')
parser.add_argument('--mp3_sources', type=str, default='../gedichteMP3/')
parser.add_argument('--destination', type=str, default='../gedichte/', help='where to put the output files')
parser.add_argument('--translations', type=str, default="en", help='list of translation language codes to search for, delimited-by-dashes')
parser.add_argument('input_files', nargs='+', help='files to be processed')

args = parser.parse_args()
if args.translations:
    args.translations = args.translations.split("-")
print(args)


langCodes = {
"Sprache: niederländisch":"nl",
"Sprache: englisch":"en",
"Sprache: deutsch":"de",
}

def cleanNodeText(node):
    #print(etree.ElementTree.tostring(node, encoding='unicode', method='xml'))
    text = re.sub(r'<.*?>', '', # remove remaining tags
           re.sub(r'(<br /> *|<p> *|</p>)', '\n', # replace br/p-tags with newlines
           re.sub(r'\n', '',  # get rid of internal newlines
                  etree.ElementTree.tostring(node, encoding='unicode', method='xml') # extract as XML
           ))).replace('\xa0', ' ' # turn non-breaking spaces into regular spaces
           ).replace('&amp;', '&').replace('&lt;', '<').replace('&gt;', '>' # revert XML entities
           ).strip() # strip leading and trailing whitespace before or after the full poem
    return text

def scrapeFile(filename):
    with codecs.open(filename, 'r', encoding='utf-8', errors='ignore') as f:
        d = html5lib.parse(f, namespaceHTMLElements=False)
    author = d.find('.//h1[@id="gedicht-autor"]').text.strip()
    assert author != None, "could not find author in {}".format(filename)
    titlenode = d.find('.//h3[@class="gedicht-originaltitel "]')
    if titlenode == None:
        titlenode = d.find('.//h3[@class="gedicht-originaltitel  gedicht-text-g"]')
    if titlenode == None:
        titlenode = d.find('.//h3[@class="gedicht-originaltitel zentriert gedicht-text-g"]')
    if titlenode == None:
        titlenode = d.find('.//h3[@class="gedicht-originaltitel blocksatz gedicht-text-g"]')
    if titlenode == None:
        titlenode = d.find('.//h3[@class="gedicht-originaltitel linksbuendig gedicht-text-g"]')
    if titlenode == None:
        titlenode = d.find('.//h3[@class="gedicht-originaltitel zentriert"]')
    if titlenode == None:
        titlenode = d.find('.//h3[@class="gedicht-originaltitel blocksatz"]')
    if titlenode == None:
        titlenode = d.find('.//h3[@class="gedicht-originaltitel linksbuendig"]')
    if titlenode == None:
        print([x.attrib for x in d.findall('.//h3')])
    assert titlenode != None, "could not find title node in {}".format(filename)
    title = cleanNodeText(titlenode)
    languageNode = d.find('.//section[@class="gedicht-rechts span9"]/header/div[@class="smallFont"]')
    assert languageNode != None, "could not find language node in {}".format(filename)
    language = langCodes[languageNode.text]
    assert language != None, "could not find language code in language node {} in file {}.".format(languageNode.text, filename)
    mp3script = d.find('.//aside[@class="gedicht-links span2 play"]/script')
    if mp3script != None:
        #mp3line = re.search(r'mp3: ".*\.mp3"', mp3script.text)[0]
        mp3line = re.search('mp3: ".*\.mp3"', mp3script.text).group(0)
        mp3 = re.sub(r'.*mp3: "(.*\.mp3)".*', r'\1', mp3line)
    else:
        mp3 = ""
    textnode = d.find('.//div[@class="gedicht-originaltext "]')
    if textnode == None:
        textnode = d.find('.//div[@class="gedicht-originaltext clearfix "]')
    assert textnode != None, "could not find text node"
#    text = etree.ElementTree.tostring(textnode, encoding='unicode', method='text').strip().replace('\xa0', " ")
    text = cleanNodeText(textnode)
    translations = {}
    for lang in args.translations:
        trans_titlenode = d.find('.//div[@id="uebersetzung-{}"]/h3'.format(lang))
        trans_textnode = d.find('.//div[@id="uebersetzung-{}"]/div[@class="gedicht-text "]'.format(lang))
        if trans_textnode == None:
            trans_textnode = d.find('.//div[@id="uebersetzung-{}"]/div[@class="gedicht-text serif "]'.format(lang))
        if trans_textnode != None:
            translations[lang] = { 'title' : cleanNodeText(trans_titlenode), 'text' : cleanNodeText(trans_textnode) }
    originnode = d.find('.//div[@id="meta-info"]')
    origin = cleanNodeText(originnode)
    if args.test_only:
        #print('in {}:\n\nauthor:\t{}\ntitle:\t{}\ntext:\n{}\n\nlanguage:\t{}\norigin:\t{}\n'.format(filename, author, title.replace('\n', '&#10;'), text, language, origin.replace('\n', '&#10;')))
        for lang in translations.keys():
            print(translations[lang]["title"] + '\n\n' + translations[lang]["text"])
    else:
        try:
            #os.mkdir(args.destination + filename)
            os.makedirs(args.destination + filename)
        except:
            print("WARN: could not create " + args.destination + filename + ", let's see what happens next...")
        with open(args.destination + filename + '/txt', 'w+') as f:
            f.write(title + '\n\n' + text)
        for lang in translations.keys():
            with open(args.destination + filename + '/txt-'+lang, 'w+') as f:
                f.write(translations[lang]["title"] + '\n\n' + translations[lang]["text"])

        allxml = '<?xml version="1.0" encoding="UTF-8"?>\n' + \
             '<article><meta>' + \
             '<link key="DC.conformsto" value="http://nats.gitlab.io/swc/schema/swc-1.0.rnc" />' + \
             '<prop key="DC.creator" value="lyriklinePageExtractor.py" />' + \
             '<prop key="DC.publisher" value="Universität Hamburg" />' + \
             '<link key="DC.reference" value="http://nbn-resolving.de/urn:nbn:de:gbv:18-228-7-2283" />' + \
             '<prop key="DC.type" value="dataset" />' + \
             '<prop key="DC.license" value="{}" />'.format(origin.replace('\n', '&#10;').replace('"', '&quot;')) + \
             '<prop key="DC.title" value="{}" />'.format(title.replace('\n', '&#10;').replace('"', '&quot;')) + \
             '<prop key="DC.language" value="{}" />'.format(language) + \
             '<prop key="DC.identifier" value="{}" />'.format(filename) + \
             '<link key="DC.source" value="https://www.lyrikline.org/de/gedichte/{}" />'.format(filename) + \
             '<link key="DC.source.text" value="https://www.lyrikline.org/de/gedichte/{}" />'.format(filename) + \
             '<link key="DC.source.audio" value="{}" />'.format(mp3) + \
             '<prop key="reader.name" value="{}" />'.format(author) + \
             '</meta><d>\n' + \
             text + '</d></article>\n'
        with open(args.destination + filename + '/swc', 'w+') as f:
            f.write(allxml)
        mp3filename = mp3.split("/")[-1] # get the filename part of URL
        try:
            if (mp3filename != ''):
                #os.symlink("../../gedichteMP3s/{}".format(mp3filename), "../gedichte/{}/audio.mp3".format(filename))
                os.symlink("../../../../quellen/lyrikline/gedichteMP3s/{}".format(mp3filename), "../gedichte/{}/audio.mp3".format(filename))
        except:
            print("WARN: could not symlink {} for file {}".format(mp3, filename))


if __name__ == '__main__':
    for filename in args.input_files:
        print("INFO: {}".format(filename))
        try:
            scrapeFile(filename)
        except Exception as excp:
            print(excp)
            print("ERRR: could not scrape file " + filename)

