#!/usr/bin/env python3

import sys

if sys.argv[1] == '--debug':
    import logging
    import http.client as http_client
    http_client.HTTPConnection.debuglevel = 1
    logging.basicConfig(level=logging.DEBUG)
    requests_log = logging.getLogger("requests.packages.urllib3")
    requests_log.setLevel(logging.DEBUG)
    requests_log.propagate = True
    sys.argv[1] = sys.argv[2]

import requests
from time import sleep
import re

s = requests.Session()

search_url = sys.argv[1]
url_base = 'https://www.lyrikline.org/de/gedichte'
assert search_url.startswith(url_base+"?")
search_name = search_url[len(url_base+"?"):]
print("searching for " + search_name)

r = s.get(url_base + "?" + search_name)
text = r.text
#print(s.cookies)
pagecount_pattern = re.compile('Seite 1 von (\d+)')
match = pagecount_pattern.search(text)
assert match != None
pagecount = int(match.group(1))

poemcount_pattern = re.compile('Gefunden: (\d+) von \d+ Gedichten')
match = poemcount_pattern.search(text)
assert match != None
search_poemcount = int(match.group(1))

print("finding {} poems on {} pages".format(search_poemcount, pagecount))

poem_pattern = re.compile('"(/de/gedichte/.*?)"')
poem_links = []
for match in poem_pattern.finditer(text):
    poem_links.append(match.group(1))

for i in range(2, pagecount+1):
    print("next page: {} of {}".format(i, pagecount))
    sleep(2)
    r = s.get(url_base + "?" + "seite="+str(i), data=dict(seite=str(i)))
    text = r.text
    poemcount_pattern = re.compile('Gefunden: (\d+) von \d+ Gedichten')
    match = poemcount_pattern.search(text)
    assert match != None
    poemcount = int(match.group(1))
    assert poemcount == search_poemcount, (poemcount, search_poemcount)

    for match in poem_pattern.finditer(text):
        poem_links.append(match.group(1))

assert search_poemcount == len(poem_links)

output = open(search_name+".poems", "w")
output.write("\n".join(poem_links))
output.write("\n") # add final newline
output.close()
'''
URL_BASE='https://www.lyrikline.org/de/gedichte/?'
SEARCH_BASE=`echo "$1" |sed 's"https://www.lyrikline.org/de/gedichte?""'`
echo "$SEARCH_BASE"
curl --verbose -j -c $COOKIE_FILE "$1" > "search-$SEARCH_BASE.0001"
# get number of follow-up pages:
NUM_PAGES=`grep "Seite 1 von " "search-$SEARCH_BASE.0001"|sed 's".*Seite 1 von "";s"</span>.*""'`
NUM_PAGES=3 # useful for debugging
for i in `seq 2 $NUM_PAGES`; do
sleep 5
curl --verbose -b $COOKIE_FILE "${URL_BASE}seite=$i" > "search-$SEARCH_BASE.`printf "%04d" $i`"
done
#rm $COOKIE_FILE
'''